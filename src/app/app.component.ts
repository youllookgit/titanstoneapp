import { Component, OnInit ,NgZone } from '@angular/core';
import { Router,RouterOutlet, ActivatedRoute, NavigationEnd } from '@angular/router';
import{ transformer } from './app-routing.animations'
import { BaseComponent } from './share/component/base.component';
import { CordovaService } from './share/service/cordova.service';
declare var device;
declare var document;
declare var jslib;
declare var window;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  // animations:[transformer]
})
export class AppComponent extends BaseComponent implements OnInit  {

  constructor(  
    public router: Router,
    public cordova : CordovaService,
    public ngzone : NgZone
  ) {
    super(); //繼承實例化
    console.log('AppComponent constructor');
  }
  ngOnInit() {
    console.log('AppComponent ngOnInit');
    //切頁畫面重置
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.AddHistory(evt.url);
      window.scrollTo(0, 0)
    });
    //add Globel Calss
    if(this.config.CordovaMode){
      var b = document.body;
      if((device.platform == "Android")){
        b.classList.add("android");
      }else{
        b.classList.add("ios");
      }
    }else{
      var b = document.body;
      b.classList.add("android");
    }

    //設定推播ID
    if(this.config.CordovaMode){
      this.SettingPushID();
    }
    //偵聽点击推送通知
    if(this.config.CordovaMode){
      this.OpenNotice();
    }
    //偵聽推播事件
    if(this.config.CordovaMode){
      this.PushEvtHandler();
    }

    //分享導頁事件
    if(this.config.CordovaMode){
      this.RedirectEvtHandler();
    }
    //導頁測試
    // setTimeout(()=>{
    //   console.log('導頁測試');
    //   this.RedirectTo('app?type=trend&id=1');
    // },3000);
    //訊息頁導頁
    // setTimeout(()=>{
    //   this.router.navigate(['/layout/msg']);
    // },3000);
    
  }
  AddHistory(url){
    console.log('AddHistory',url);
    if(!this.UrlIgnore(url)){
      var list = this.jslib.GetStorage('history');
      
      if(typeof list == 'undefined'){
        list = (list) ? list : [url];
        this.jslib.SetStorage('history',list);
      }
      if(list[list.length-1] !=  url){
        //若網址為重複類型 取代最後一筆
        var _last = (list[list.length-1]) ? (list[list.length-1].split('/')) : [];
        var _now = url.split('/');
        var _lasturl = '';
        //只有參數為id類須省略
        if(_last.length > 0 && Number(_last[_last.length - 1]) > 0){
          _lasturl =  _last.splice(0,_last.length-1).join('/');
        }else{
          _lasturl = _last.join('/');
        }
        var _nowurl = '';
        if(_now.length > 0 && Number(_now[_now.length - 1]) > 0){
          _nowurl =  _now.splice(0,_now.length-1).join('/');
        }else{
          _nowurl = _now.join('/');
        }

        list =  (list) ? list : [];
        
        if(list.length > 2 && _lasturl == _nowurl){
          list[list.length-1] = url;
        }else{
          list.push(url);
        }
        //keep 30
        if(list.length > 30){
          list = list.slice(1);
        }
        this.jslib.SetStorage('history',list);
      }
    }
  }
  UrlIgnore(url){
    return false;
    // var result = false;
    // return (
    //   (url == '/') ||
    //   (url == '/intro') ||
    //   (url == '/init')
    // );
  }
  //取得推播ID
  SettingPushID(){
    var _pushid = jslib.GetStorage('pushid');
    if(_pushid == null || typeof _pushid == 'undefined' || _pushid == ''){
      window['JPush'].getRegistrationID((rId) => {
        console.log("JPushPlugin:取得推播ID為" + rId);
        if(rId &&  rId!= ''){
          
          jslib.SetStorage('pushid',rId);
          var _notice = {
            DeviceID:device.uuid,
            Platform:device.platform,
            Version:device.version,
            PushID:rId,
            Lang:jslib.GetStorage('lang')
          };
          console.log('RegistNotice 建立初次推播權限項目內容',_notice);
          if(_notice.PushID){
            this.baseApi.backPost('User/RegistNotice',_notice).then((res)=>{
              this.jslib.SetStorage('notice',res);
            });
          }
        }else{
          //一秒鐘後重取
          setTimeout(() => {
            console.log('AppComponent 重取推播ID');
            this.SettingPushID();
          },1000);
        }
      });

    }else{
      console.log('AppComponent 已有推播ID，不再重取，發送更新紀錄');
      var _notice = {
        DeviceID:device.uuid,
        Platform:device.platform,
        Version:device.version,
        PushID:_pushid,
        Lang:jslib.GetStorage('lang')
      };
      console.log('RegistNotice 建立初次推播權限項目內容',_notice);
      if(_notice.PushID){
        this.baseApi.backPost('User/RegistNotice',_notice).then((res)=>{
          this.jslib.SetStorage('notice',res);
        });
      }
      if(_notice.DeviceID == 'android'){
        window['JPush'].init((_inint) =>{
          console.log("JPushPlugin:初始化",_inint);
        });
      }
    }
  }
  //点击推送通知 启动或唤醒应用程序时会触发该事件
  OpenNotice(){
    document.addEventListener("jpush.openNotification",  (event) => {
      console.log('openNotification');
      //轉至msg頁
      this.router.navigate(['/layout/msg']);
    }, false);
  }
  //偵聽推播事件
  PushEvtHandler(){
    console.log('AppComponent 偵聽推播事件');
    document.addEventListener("jpush.receiveNotification",  (event) => {
      console.log('GET PUSH',event);
      // if(device.platform == "Android") {
      //   if(event['extras']){}
      // } else {
      //   var jmsg = event.aps.alert;
      //   if(event['extras']){}
      // }
      console.log('通知推播更新Service');
      this.cordova.setPushUpdate(true);
    }, false);
  }

  //分享導頁事件
  RedirectEvtHandler(){
    console.log('AppComponent 分享導頁事件');
    window['handleOpenURL'] = (url) => {
      //若大於3秒會有收不到的機率
      setTimeout(() => {
        console.log("received url: " + url);
        this.ngzone.run(()=>{
          this.RedirectTo(url);
        });
      }, 250);
    }
  }

  RedirectTo(redirect){
    if(redirect && redirect != ''){
      console.log('redirect',redirect);
      var _url = redirect.split('?');
      if(_url.length == 2){
        var param = _url[1];
        var _p = param.split('&');
        if(_p.length == 2){
          var _type = _p[0].replace('type=','');
          var _id = _p[1].replace('id=','');
          //case 地產
          if(_type == 'estateitem' && Number(_id) > 0){
            this.router.navigate(['/layout/estate/item/' + _id]);
          }
          //case 趨勢新聞
          if(_type == 'trend' && Number(_id) > 0){
            this.router.navigate(['/layout/trend/detail/' + _id]);
          }
          //case 說明會
          if(_type == 'seminar' && Number(_id) > 0){
            this.router.navigate(['/layout/seminar/detail/' + _id]);
          }
          //case 官方新聞
          if(_type == 'news' && Number(_id) > 0){
            this.router.navigate(['/layout/news/detail/' + _id]);
          }
        }
      }
    }
  }

}
