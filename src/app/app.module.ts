// ==angular== //
import { NgModule,Injector } from '@angular/core';
import { BrowserModule} from '@angular/platform-browser';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

// import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { Title } from '@angular/platform-browser';

// Import components
import { AppComponent } from './app.component';
import { BaseComponent }  from './share/component/base.component';
// Loading Component
import { LoadingComponent } from './share/component/loading.component';
import { PopConfirmComponent } from './share/component/pop-confirm.component';
// Page Component
import { HomeComponent }  from './component/home/home.component';
import { LayoutComponent }  from './component/layout/layout.component';
import { InitComponent }  from './component/init/init.component';
import { IntroComponent }  from './component/intro/intro.component';
import { FindComponent }  from './component/find/find.component';
import { EtateComponent }  from './component/estate/estate.component';
import { EtateItemComponent }  from './component/estate/estateItem.component';
import { LoginComponent }  from './component/login/login.component';

import { MyassetsComponent } from './component/myassets/myassets.component';
import { MyEstateComponent } from './component/myassets/estate.component';
import { EstateEmptyComponent } from './component/myassets/estateEmpty.component';
import { EstateFinishComponent } from './component/myassets/estateFinish.component';
import { EstateUnFinishComponent } from './component/myassets/estateUnFinish.component';

//20200309 add Fund
import { FundComponent } from './component/fund/fund.component';
import { FunditemComponent } from './component/fund/funditem.component';
import { FundHisComponent } from './component/fund/fundhis.component';
import { FundChartComponent } from './component/fund/fund.chart';
//20200405 add Insurance
import { InsuranceComponent } from './component/insurance/insurance.component';
import { InsuranceitemComponent } from './component/insurance/insuranceitem.component';

import { NewsComponent } from './component/news/news.component';
import { NewsDetailComponent } from './component/news/newsDetail.component';
import { FaqComponent }  from './component/faq/faq.component';
import { FaqItemsComponent }  from './component/faq/faqitem.component';
import { SeminarComponent } from './component/seminar/seminar.component';
import { SeminarDetailComponent } from './component/seminar/seminarDetail.component';
import { SeminarSignComponent } from './component/seminar/seminarSign.component';
import { TrendComponent } from './component/trend/trend.component';
import { TrendDetailComponent } from './component/trend/trendDetail.component';

import { ForgetComponent }  from './component/login/forget.component';
import { RegistComponent }  from './component/login/regist.component';
import { Regist2Component }  from './component/login/regist2.component';
import { Regist3Component }  from './component/login/regist3.component';
import { Regist4Component }  from './component/login/regist4.component';

import { MsgComponent } from './component/msg/msg.component';
import { ContactusComponent } from './component/contactus/contactus.component';
//set
import { SettingComponent } from './component/setting/setting.component';
import { PersonComponent } from './component/setting/person.component';
import { ResetpwdComponent } from './component/setting/resetpwd.component';
import { PreferComponent } from './component/setting/prefer.component';
import { NoticeComponent } from './component/setting/notice.component';
import { VersionComponent } from './component/setting/version.component';
import { LangComponent } from './component/setting/lang.component';
import { AboutComponent }  from './component/setting/about.component';
import { TermsComponent }  from './component/setting/terms.component';
//Page Part Component
import { EstateSliderComponent }  from './component/home/estate.slider';
import { SeminarSliderComponent }  from './component/home/seminar.slider';
import { TrendSliderComponent }  from './component/home/trend.slider';
import { NewsListComponent }  from './component/home/news.list';
import { EsFlrSliderComponent }  from './component/estate/floor.slider';
import { EsDvsSliderComponent }  from './component/estate/device.slider';
import { EsItrSliderComponent }  from './component/estate/intro.slider';
import { LineChartComponent }  from './component/myassets/line.chart';
import { CircleChartComponent }  from './component/myassets/circle.chart';
import { WorkSliderComponent }  from './component/myassets/work_pic.slider';
import { PayRecordComponent }  from './component/myassets/pay_record.slider';
// Share Module
import { ShareModule } from './share/share.module';
// Pipe
import { PipeModule } from '../app/share/pipe/pipe.module';
// Directuve
import { InputLitmitDirective } from './share/directive/inputDirective';
import { MapDirective } from './share/directive/mapDirective';
import { CallPhoneDirective } from './share/directive/phoneDirective';
import { UrlFileDirective } from './share/directive/urlfileDirective';
import { ShareDirective } from './share/directive/shareDirective';
import { LineShareDirective } from './share/directive/lineshareDirective';
// const Components
const APP_CONTAINERS = [
  BaseComponent,
  HomeComponent,
  LayoutComponent,
  InitComponent,
  IntroComponent,
  FindComponent,
  EtateComponent,
  EtateItemComponent,
  
  MyassetsComponent,
  MyEstateComponent,
  EstateEmptyComponent,
  EstateFinishComponent,
  EstateUnFinishComponent,

  FundComponent,
  FunditemComponent,
  FundHisComponent,
  FundChartComponent,
  InsuranceComponent,
  InsuranceitemComponent,
  
  NewsComponent,
  NewsDetailComponent,
  FaqComponent,
  FaqItemsComponent,
  SeminarComponent,
  SeminarDetailComponent,
  SeminarSignComponent,
  TrendComponent,
  TrendDetailComponent,
  
  LoginComponent,
  ForgetComponent,
  RegistComponent,
  Regist2Component,
  Regist3Component,
  Regist4Component,

  MsgComponent,
  ContactusComponent,

  SettingComponent,
  PersonComponent,
  ResetpwdComponent,
  PreferComponent,
  NoticeComponent,
  VersionComponent,
  LangComponent,
  AboutComponent,
  TermsComponent,
//Page Part Component
  EstateSliderComponent,
  SeminarSliderComponent,
  TrendSliderComponent,
  NewsListComponent,
  EsFlrSliderComponent,
  EsDvsSliderComponent,
  EsItrSliderComponent,
  LineChartComponent,
  CircleChartComponent,
  WorkSliderComponent,
  PayRecordComponent,
];
import { AppRoutingModule } from './app-routing.module';
// import { notImplemented } from '@angular/core/src/render3/util';
@NgModule({
  imports: [
    // ==angular== //
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ShareModule,
    PipeModule,
  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    LoadingComponent,
    PopConfirmComponent,
    //directuve
    InputLitmitDirective,
    MapDirective,
    CallPhoneDirective,
    UrlFileDirective,
    ShareDirective,
    LineShareDirective
  ],
  providers: [
    // TelegramService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  static injector: Injector;//DI 注入結構宣告
  constructor(
    injector: Injector//DI 注入結構宣告
  ) {
    AppModule.injector = injector;//DI 注入結構宣告
  }
}
