import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }  from './component/home/home.component';
import { LayoutComponent }  from './component/layout/layout.component';
import { InitComponent }  from './component/init/init.component';
import { IntroComponent }  from './component/intro/intro.component';
import { FindComponent }  from './component/find/find.component';

import { EtateComponent }  from './component/estate/estate.component';
import { EtateItemComponent }  from './component/estate/estateItem.component';

import { LoginComponent }  from './component/login/login.component';

import { ForgetComponent }  from './component/login/forget.component';
import { RegistComponent }  from './component/login/regist.component';
import { Regist2Component }  from './component/login/regist2.component';
import { Regist3Component }  from './component/login/regist3.component';
import { Regist4Component }  from './component/login/regist4.component';

import { MyassetsComponent } from './component/myassets/myassets.component';
import { MyEstateComponent } from './component/myassets/estate.component';
import { EstateEmptyComponent } from './component/myassets/estateEmpty.component';
import { EstateFinishComponent } from './component/myassets/estateFinish.component';
import { EstateUnFinishComponent } from './component/myassets/estateUnFinish.component';

//20200309 add Fund
import { FundComponent } from './component/fund/fund.component';
import { FunditemComponent } from './component/fund/funditem.component';
import { FundHisComponent } from './component/fund/fundhis.component';
//20200405 add Insurance
import { InsuranceComponent } from './component/insurance/insurance.component';
import { InsuranceitemComponent } from './component/insurance/insuranceitem.component';

import { NewsComponent } from './component/news/news.component';
import { NewsDetailComponent } from './component/news/newsDetail.component';
import { FaqComponent }  from './component/faq/faq.component';
import { FaqItemsComponent }  from './component/faq/faqitem.component';
import { SeminarComponent } from './component/seminar/seminar.component';
import { SeminarDetailComponent } from './component/seminar/seminarDetail.component';
import { SeminarSignComponent } from './component/seminar/seminarSign.component';
import { TrendComponent } from './component/trend/trend.component';
import { TrendDetailComponent } from './component/trend/trendDetail.component';

import { MsgComponent } from './component/msg/msg.component';
import { ContactusComponent } from './component/contactus/contactus.component';

import { SettingComponent } from './component/setting/setting.component';
import { PersonComponent } from './component/setting/person.component';
import { ResetpwdComponent } from './component/setting/resetpwd.component';
import { PreferComponent } from './component/setting/prefer.component';
import { NoticeComponent } from './component/setting/notice.component';
import { VersionComponent } from './component/setting/version.component';
import { LangComponent } from './component/setting/lang.component';

import { AboutComponent }  from './component/setting/about.component';
import { TermsComponent }  from './component/setting/terms.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'init',
    pathMatch: 'full',
  },
  {
    path: 'intro',
    component: IntroComponent,
  },

  {
    path: 'init',
    component: InitComponent,
  },
  {
    path: 'layout',
    component: LayoutComponent,
    // data:{animation:'transformer'},
    children: [
      //global page
      { path: 'msg', component: MsgComponent,data:{animation:'isLeft'} },
      { path: 'contactus', component: ContactusComponent,data:{animation:'isRight'} },
      //estate
      { path: 'estate/item/:id', component: EtateItemComponent, data: {animation: 'isLeft',tab:1} },
      //my estate
      { path: 'myestate', component: MyEstateComponent,data:{animation:'isRight',tab:1} },
      { path: 'estateEmpty', component: EstateEmptyComponent,data:{animation:'isRight',tab:1} },
      { path: 'estateFinish/:id', component: EstateFinishComponent,data:{animation:'isLeft',tab:1} },
      { path: 'estateUnFinish/:id', component: EstateUnFinishComponent,data:{animation:'isLeft',tab:1} },
      //my fund
      { path: 'myfund', component: FundComponent,data:{animation:'isRight',tab:1} },
      { path: 'myfunditem/:id', component: FunditemComponent,data:{animation:'isLeft',tab:1} },
      { path: 'myfundhis/:id', component: FundHisComponent,data:{animation:'isRight',tab:1} },
      //my insurance
      { path: 'insurance', component: InsuranceComponent,data:{animation:'isRight',tab:1} },
      { path: 'insuranceitem/:id', component: InsuranceitemComponent,data:{animation:'isLeft',tab:1} },
      //trend
      { path: 'trend/detail/:id', component: TrendDetailComponent,data:{animation:'isRight',tab:3} },
      { path: 'trend', component: TrendComponent,data:{animation:'isLeft',tab:3} },
      //news
      { path: 'news/detail/:id', component: NewsDetailComponent,data:{animation:'isRight',tab:3} },
      { path: 'news', component: NewsComponent,data:{animation:'isLeft',tab:3} },
      //seminar
      { path: 'seminar/Sign/:id', component: SeminarSignComponent,data:{animation:'isLeft'} },
      { path: 'seminar/detail/:id', component: SeminarDetailComponent,data:{animation:'isRight'} },
      { path: 'seminar', component: SeminarComponent,data:{animation:'isLeft',tab:3} },
      //set
      { path: 'person', component: PersonComponent,data:{animation:'isRight'} },
      { path: 'resetpwd', component: ResetpwdComponent,data:{animation:'isRight'} },
      { path: 'prefer', component: PreferComponent,data:{animation:'isRight'} },
      { path: 'notice', component: NoticeComponent,data:{animation:'isRight'} },
      
      { path: 'version', component: VersionComponent,data:{animation:'isRight'} },
      { path: 'lang', component: LangComponent,data:{animation:'isRight'} },
      //login
      { path: 'login', component: LoginComponent, data: {animation: 'isLeft'} },
      { path: 'forget', component: ForgetComponent,data:{animation:'isRight'} },
      { path: 'regist', component: RegistComponent,data:{animation:'isRight'} },
      { path: 'regist2', component: Regist2Component,data:{animation:'isRight'} },
      { path: 'regist3', component: Regist3Component,data:{animation:'isRight'} },
      { path: 'regist4', component: Regist4Component,data:{animation:'isRight'} },

      { path: 'faq/item/:id', component: FaqItemsComponent,data:{animation:'isLeft',tab:4}},
      { path: 'faq',component: FaqComponent,data:{animation:'isRight',tab:4}},
     
      //bottom menu
      { path: 'home', component: HomeComponent, data: {animation: '1',tab:0} },
      { path: 'myassets', component: MyassetsComponent, data: {animation: '2',tab:1} },
      { path: 'estate', component: EtateComponent, data: {animation: '3',tab:2} },
      { path: 'find', component: FindComponent, data: {animation: '4',tab:3} },
      { path: 'set', component: SettingComponent,data:{animation:'5',tab:4} },
      {path: 'about',component: AboutComponent, data: {animation: 'isRight'}},
      {path: 'terms',component: TermsComponent, data: {animation: 'isRight'}}
    ]
  }
  // ,{
  //   path: 'home',
  //   component: HomeComponent,
  //   data:{animation:'FilterPage'}
  // }
  // ,{
  //   path: 'faq',
  //   component: FaqComponent,
  //   children: [
  //     { path: 'item', component: FaqItemsComponent, data: {animation: 'isLeft'} }
  //   ]
  // }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
