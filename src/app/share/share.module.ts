// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { ApiService } from './service/api.service';
import { PopupService } from './service/popup.service';
import { CordovaService } from './service/cordova.service';
import { TopBarService } from './service/topBar.service';

//import { PopService } from './service/pop.service';

// Modal Component
//import { PopComponent } from './pop/pop.component'
import { I18nService } from './service/i18n.service';
//Component
@NgModule({
  imports: [
    CommonModule,
    //ModalModule,
    FormsModule
  ],
  declarations: [
  ],
  providers: [
    ApiService,
    //ConfigService,
    PopupService,
    CordovaService,
    I18nService,
    TopBarService,
    //BsModalService,
    //PopService
  ],
  exports: [
    //PopTransQueryComponent
  ]
})
export class ShareModule { }
