import { Pipe, PipeTransform} from '@angular/core';
import {I18nService} from '../service/i18n.service'
@Pipe({
  name: 'i18n'
})
export class I18nPipe implements PipeTransform {

  constructor(  
    public i18n : I18nService
  ) {
    //console.log('I18nPipe constructor');
  }
  transform(key: any): any {
    //console.log('I18nPipe transform',key,lang);
    var nowLang = this.i18n.getLang();
    var keys = key.split('.');
    if(keys.length > 1){
      var result = nowLang[keys[0]][keys[1]];
      if(result){
        return result;
      }else{
        return key;
      }
    }else{
      var result = nowLang[key];
      if(result){
        return result;
      }else{
        return key;
      }
    }

  }
}
