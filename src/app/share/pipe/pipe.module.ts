// Angular
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
//Pipe
import { I18nPipe } from './i18n.pipe';
import {NumberPipe} from './Number.pipe';
import {IdNoPipe} from './Idno.pipe';
import {HtmlHandlerPipe} from './HtmlHandler.pipe';
import { SafePipe } from './Safe.pipe';
import { SafeHtmlPipe } from './SafeHtml.pipe';
@NgModule({
  imports: [
  ],
  declarations: [
    I18nPipe,
    NumberPipe,
    IdNoPipe,
    HtmlHandlerPipe,
    SafePipe,
    SafeHtmlPipe
  ],
  providers: [
  ],
  exports: [I18nPipe,NumberPipe,IdNoPipe,HtmlHandlerPipe,SafePipe,SafeHtmlPipe]
})
export class PipeModule { }
