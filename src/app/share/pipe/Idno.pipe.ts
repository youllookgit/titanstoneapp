import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer} from '@angular/platform-browser';
@Pipe({
  name: 'Idno'
})
export class IdNoPipe implements PipeTransform {
  constructor() {}
  transform(prama) {
    if(prama){
      if(prama.length >= 3){
        return prama.slice(0,prama.length-3) + '***';
      }else{
        return prama;
      }
    }else{
      return '';
    }
  }
} 
