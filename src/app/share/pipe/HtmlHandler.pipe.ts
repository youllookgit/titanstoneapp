import { Pipe, PipeTransform } from '@angular/core';
import {DomSanitizer} from "@angular/platform-browser";
declare var jslib:any; //呼叫第三方js

@Pipe({
  name: 'htmlHandle'
})
export class HtmlHandlerPipe implements PipeTransform {
  constructor(  
    public sanitized: DomSanitizer
  ) {
  }
  transform(value: any): any {
    //console.log('HtmlHandlerPipe',value);
    //value = jslib.replaceAll(value,'src','[src]');
    return this.sanitized.bypassSecurityTrustHtml(value);//value;
  }
}
