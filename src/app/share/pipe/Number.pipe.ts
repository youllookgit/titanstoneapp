import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'noTrans'
})
export class NumberPipe implements PipeTransform {

  transform(value: any): any {
      if(value == null){
        return '-';
      }
      var _n = Number(value);
      if(_n != NaN){
        var _format = (_n).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
        return _format;
      }else{
        return value;
      }
    
  }
}
