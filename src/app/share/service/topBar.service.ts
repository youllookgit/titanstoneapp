import { Injectable, ViewChild ,EventEmitter } from '@angular/core';

@Injectable()
export class TopBarService {
  public eventUpdated:EventEmitter<any> = new EventEmitter();
  public shareEvtUpd:EventEmitter<any> = new EventEmitter();
  constructor() { 
    
  }
  set(obj) {
    this.eventUpdated.emit(obj)
  };
  setBgClass(_name:any) {
    this.eventUpdated.emit({
      bg:_name
    })
  };
  setHeaderClass(_name:any) {
    this.eventUpdated.emit({
      header:_name
    })
  };
  setHeaderTxt(_txt:any) {
    this.eventUpdated.emit({
      txt:_txt
    })
  };
  setBack(set:any) {
    this.eventUpdated.emit({
      name:'back',
      func:set
    })
  };
  clearBack() {
    console.log('setBack');
    this.eventUpdated.emit({
      name:'back',
      func:null
    })
  };
  OpenShare(_url){
    this.shareEvtUpd.emit(_url)
  }

}
