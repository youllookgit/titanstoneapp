import { Injectable } from '@angular/core';
//import { Http, Response } from '@angular/http';
import { HttpClient, HttpResponse,HttpHeaders } from '@angular/common/http';
import { PopupService } from './popup.service';
import { Config } from '../../../assets/configuration/config';
declare var jslib:any; //呼叫第三方js

@Injectable()

export class ApiService {

  baseUrl = Config.releaseUrl;
  lang = Config.DefaultLang;
  constructor(
    private http: HttpClient,
    private loading: PopupService
  ) {
     if(Config.DebugMode){
      this.baseUrl = Config.DevUrl;
     }
   }
  apiPost(url:string,param:any){
    this.loading.setLoading(true);
    var body = (param) ? param : {};

    return new Promise((resolve,reject)=>{
        var _url = this.baseUrl + url;
        this.http.post(_url,body,this.getOption()).toPromise().then(
          (res : Response) => {
            this.loading.setLoading(false);
            console.log(_url,res);
            var _result = res['_body'] || '';
            if(_result != ''){
              res = JSON.parse(_result);
            }
            if(res['Success']){
              resolve(res['data']);
            }else{
              this.loading.setConfirm({
                content:res['msg']
              });
              reject(res);
            }
          },
          (error: Response) => {
            this.loading.setLoading(false);
            if(error['status'] != 200){
              var _error = {
                url:error['url'],
                status:error['status'],
                errormsg:error['statusText']
              }
              console.log('[API Error]',_error);
              reject(_error);
            }else{
              reject(error.json());
            }
          }
        );

    });
    
  }
  apiPostFile(url:string,file:File){

    this.loading.setLoading(true);

    var _formData :FormData = new FormData();
    _formData.append("upload", file);
    console.log('[api Resquest',this.baseUrl + url, _formData);
    return new Promise((resolve,reject)=>{
      this.http.post(this.baseUrl + url, _formData,this.getOption()).toPromise().then(
        (res) => {
          this.loading.setLoading(false);
          console.log('[Repsonese]',res);
          if(res['Success']){
            resolve(res['filepath']);
          }else{
            reject(res);
          }
        },
        (error: Response) => {
          this.loading.setLoading(false);
          console.log('error',error);
          reject(error.json());
        }
      );
    });
    
  }
  //隱藏讀取動畫
  backPost(url:string,param:any){
    var body = (param) ? param : {};
    console.log('[api Resquest]',url,body,this.getOption());
    return new Promise((resolve,reject)=>{
        var _url = this.baseUrl + url;
        this.http.post(_url,body,this.getOption()).toPromise().then(
          (res : Response) => {
            console.log('Result',res);
            var _result = res['_body'] || '';
            if(_result != ''){
              res = JSON.parse(_result);
            }
            if(res['Success']){
              resolve(res['data']);
            }else{
              reject(res);
            }
          },
          (error: Response) => {
            this.loading.setLoading(false);
            if(error['status'] != 200){
              var _error = {
                url:error['url'],
                status:error['status'],
                errormsg:error['statusText']
              }
              console.log('[API Error]',_error);
              reject(_error);
            }else{
              reject(error.json());
            }
          }
        );

    });
    
  }

  getOption(){
    var optionObj = {
      'lang':this.lang,
      'Token':'',
      // 'Content-Type':'application/json'
    };

    //語系參數
    var _lang = jslib.GetStorage('lang');
    if(typeof _lang != 'undefined'){
      optionObj.lang = _lang;
    }
    var user = jslib.GetStorage('user');
    if(user){
      optionObj.Token = user['Token'];
    }
    var headers = new HttpHeaders(optionObj);
    let options = {
      headers:headers
    };
    return options;
  }

}
