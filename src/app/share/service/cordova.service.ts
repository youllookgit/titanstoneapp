import { Injectable, ViewChild ,EventEmitter } from '@angular/core';

@Injectable()
export class CordovaService {
  public GetPushID:EventEmitter<any> = new EventEmitter();
  public pushUpdated:EventEmitter<any> = new EventEmitter();
  public redirectPage:EventEmitter<any> = new EventEmitter();
  constructor() { 
    
  }
  //觸發推播事件更新
  setPushUpdate(data:any) {
    this.pushUpdated.emit(data);
  }
  //觸發轉頁事件
  setRedirectTo(data:any) {
    this.redirectPage.emit(data);
  }
}
