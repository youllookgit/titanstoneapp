import { Injectable, ViewChild ,EventEmitter } from '@angular/core';

//langs
import {Config} from '../../../assets/configuration/config';
import {EN_US_TRANS} from '../../../assets/configuration/lang/en_US'
import {ZH_CN_TRANS} from '../../../assets/configuration/lang/zh_CN'
declare var jslib:any; //呼叫第三方js

@Injectable()
export class I18nService {
  public eventUpdated:EventEmitter<boolean> = new EventEmitter();
  public Langs:any;
  public nowLang;
  constructor() {
    this.Langs = {
      'en_US' : EN_US_TRANS,
      'zh_CN' : ZH_CN_TRANS
    };
    var _lang = jslib.GetStorage('lang');
    _lang = (_lang) ? _lang : Config.DefaultLang;
    console.log('nowLang',_lang);
    this.nowLang = this.Langs[_lang];
  }
  
  setLang(langCode) {
    console.log('setLang',langCode);
    if(this.Langs[langCode]){
      jslib.SetStorage('lang',langCode);
      this.nowLang = this.Langs[langCode];
    }
  }

  getLang(){
    if(this.nowLang){
      return this.nowLang;
    }else{
      var _lang = jslib.GetStorage('lang');
      _lang = (_lang && this.Langs[_lang]) ? _lang : Config.DefaultLang;
      this.nowLang = this.Langs[_lang];
      return this.nowLang;
    }
  }
  getLangCode(){
    var _lang = jslib.GetStorage('lang');
    _lang = (_lang) ? _lang : Config.DefaultLang;
    return _lang;
  }

  Trans(key){
    var nowLang = this.getLang();
    var keys = key.split('.');
    if(keys.length > 1){
      var result = nowLang[keys[0]][keys[1]];
      if(result){
        return result;
      }else{
        return key;
      }
    }else{
      var result = nowLang[key];
      if(result){
        return result;
      }else{
        return key;
      }
    }
  }
}
