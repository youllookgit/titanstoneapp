import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import {Config} from '../../../assets/configuration/config';
declare var startApp;
declare var device;
declare var cordova;
@Directive(
    {
         selector: '[lineUrl]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class LineShareDirective {
    public url = '';
    constructor(
    ) {
    }
    @Input() set lineUrl(param){
        this.url = param;
    }
    preventDefault(event) {
        console.log('lineshareUrl url:',this.url);
        var rooturl = 'http://titanstone.webmaster.net.tw/Home/Share?' + this.url;
        var line_url = 'http://line.naver.jp/R/msg/text/?' + encodeURI(rooturl);
        if(Config.CordovaMode){
            if(device.platform == "Android"){
                startApp.set({ 
                    "action": "ACTION_VIEW",
                    "uri": line_url
                }).start();
            }else{
                //ios
                var sApp = startApp.set(line_url);
                sApp.start();
            }
        }
        event.stopPropagation();
    }
}
