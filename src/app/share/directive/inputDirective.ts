import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import { NgControl } from "@angular/forms";
import { Router} from '@angular/router';
@Directive(
    {
         selector: '[inputlitmit]',
         host : {
            '(keyup)' : 'preventDefault($event)'
          }
    }
)
export class InputLitmitDirective {
    // @Input() href;
    constructor(
        public el: ElementRef,
        public formCtrl : NgControl
    ) {
    }
    preventDefault(event) {
        //https://codertw.com/%E5%89%8D%E7%AB%AF%E9%96%8B%E7%99%BC/243090/
        let _value = this.el.nativeElement.value;
        _value = _value.replace(/[\W]/g,'');//只能英文或數字
        this.formCtrl.control.setValue(_value);
    }
}