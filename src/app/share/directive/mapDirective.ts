import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import {Config} from '../../../assets/configuration/config';
declare var startApp;
declare var device;
declare var jslib;
@Directive(
    {
         selector: '[openMap]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class MapDirective {
    public url = '';
    constructor(
        public el: ElementRef
    ) {
    }
    @Input() set openMap(param){
        this.url = param;
    }
    preventDefault(event) {
        let _value = this.el.nativeElement.innerText;
        if(this.url != ''){
            _value = this.url;
        }
        console.log('addr',_value);
        var _praram = encodeURI(_value);
       
        if(Config.CordovaMode){

            var mapSchema = jslib.GetStorage('map');

            if(device.platform == "Android"){

                startApp.set({
                    "action": "ACTION_VIEW",
                    "uri": mapSchema + _praram,
                    "intentstart":"startActivity"
                }).start();

            }else{
                //ios
                var iosmap = startApp.set(mapSchema + _praram);
                iosmap.start();
            }
            //ANDROID 若用原生 SCHEMA: geo: 則可自己選地圖
            //https://blog.csdn.net/github_34460372/article/details/79549330
            //https://www.npmjs.com/package/com.lampa.startapp
            // var sApp = startApp.set({
            //     "action":"ACTION_VIEW",
            //     "category":"CATEGORY_DEFAULT",
            //     //"type":"text/css",
            //     "package":"com.baidu.BaiduMap",
            //     "uri":"google.navigation:q=" + encodeURI(_value),
            //     //"flags":["FLAG_ACTIVITY_CLEAR_TOP","FLAG_ACTIVITY_CLEAR_TASK"],
            //     //"intentstart":"startActivity"
            // });
        }
        event.stopPropagation();
    }
}
