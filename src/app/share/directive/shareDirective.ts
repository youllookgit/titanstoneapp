import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import {Config} from '../../../assets/configuration/config';
import { PopupService } from '../../share/service/popup.service';
declare var startApp;
declare var device;
declare var cordova;
@Directive(
    {
         selector: '[shareUrl]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class ShareDirective {
    public url = '';
    constructor(
        public pop: PopupService
    ) {
    }
    @Input() set shareUrl(param){
        this.url = param;
    }
    preventDefault(event) {

        console.log('shareUrl url:',this.url);
        var rooturl = 'http://titanstone.webmaster.net.tw/Home/Share?' + this.url;
       
        if(Config.CordovaMode && cordova.plugins.clipboard){
            cordova.plugins.clipboard.copy(rooturl,(success)=>{
                console.log('success',success);
                this.pop.setConfirm({
                    content:'已將分享網址拷貝至剪貼簿！'
                  });
            });
        }
        event.stopPropagation();
    }
}
