import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import {Config} from '../../../assets/configuration/config';
declare var startApp;
declare var device;
@Directive(
    {
         selector: '[callPhone]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class CallPhoneDirective {
    public phone = '';
    constructor(
      //  public el: ElementRef
    ) {
    }
    @Input() set callPhone(param){
        this.phone = param;
    }
    preventDefault(event) {
        let _value = this.phone;
        console.log('callPhone',_value);
        if(Config.CordovaMode){
            if(device.platform == "Android"){
                startApp.set({
                    "action": "ACTION_VIEW",
                    "uri": "tel:" + _value
                }).start();
            }else{
                //ios
                var sApp = startApp.set("tel:" + _value);
                sApp.start();
            }
            
        }
        event.stopPropagation();
    }
}
