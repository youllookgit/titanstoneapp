import { Directive, ElementRef, HostListener, Input, Inject } from '@angular/core';
import {Config} from '../../../assets/configuration/config';
declare var startApp;
declare var device;
@Directive(
    {
         selector: '[openUrl]',
         host : {
            '(click)' : 'preventDefault($event)'
          }
    }
)
export class UrlFileDirective {
    public url = '';
    constructor(
       // public el: ElementRef
    ) {
    }
    @Input() set openUrl(param){
        this.url = param;
    }
    preventDefault(event) {
        console.log('openUrl url:',this.url);
        if(Config.CordovaMode){
            var time = 100;
            //延遲一秒再開WeChat
            time = (this.url == 'weixin://') ? 1000 : time;
            setTimeout(()=>{
                if(device.platform == "Android"){
                    startApp.set({ 
                        "action": "ACTION_VIEW",
                        "uri": this.url
                    }).start();
                }else{
                    //ios
                    var sApp = startApp.set(this.url);
                    sApp.start();
                }
            },time);
        }
        event.stopPropagation();
    }
}
