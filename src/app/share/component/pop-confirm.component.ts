
import { Component } from '@angular/core';
import { PopupService } from '../service/popup.service'

@Component({
  selector: 'app-pop-confirm',
  templateUrl: './pop-confirm.component.html'
})
export class PopConfirmComponent {
  public _subscribe;
  
  public open = false;
  public title = '';
  public content = '';
  public cancelTxt = '';
  public checkTxt = '';
  public event: any;
  public cancelEvent: any;

  constructor(
    public pop: PopupService
  ) {
    console.log('PopConfirmComponent constructor');
    this._subscribe = this.pop.confirnSetting.subscribe(
      (setting: object) => {
        this.open = true;
        this.title = setting['title'] || '';
        this.content = setting['content'] || '';
        this.checkTxt = setting['checkTxt'] || 'A114.006';
        this.cancelTxt = setting['cancelTxt'] || '';
        this.event = setting['event'];
        this.cancelEvent = setting['cancelEvent'];
      }
    );
  }

  Cancel() {
    this.open = false;
    if(typeof this.cancelEvent == 'function'){
      this.cancelEvent();
    }
  }

  Submit() {
    if (typeof this.event == 'function') {
      this.event(true);
    }
    this.open = false;
  }
  ngOnDestroy() {
    this._subscribe.unsubscribe();
  }
}




