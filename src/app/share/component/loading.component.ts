import { Component, OnInit } from '@angular/core';
import { PopupService } from '../service/popup.service';
@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html'
})
export class LoadingComponent implements OnInit {
  public Isopen = false;
  public className = 'loadingFadeOut';
  constructor(private loadservice : PopupService) { }

  ngOnInit() {
    this.loadservice.eventUpdated.subscribe(
      () => {
        this.Isopen = this.loadservice.getLoading();
        if(this.loadservice.getLoading()){
          this.className = 'loadingFadeIn';
        }else{
          this.className = 'loadingFadeOut';
        }
      }
    );
  }
}
