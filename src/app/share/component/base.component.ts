import { Component, OnInit , EventEmitter} from '@angular/core';
import { I18nService } from '../../share/service/i18n.service';
import { ApiService } from '../../share/service/api.service';
import { TopBarService } from '../../share/service/topBar.service';
import { PopupService } from '../../share/service/popup.service';
import { AppModule } from '../../app.module';
import { Router} from '@angular/router';
import { Config } from '../../../assets/configuration/config';
declare var jslib:any; //呼叫第三方js
declare var document;
declare var device;
@Component({
  template: ''
})
export class BaseComponent{
  public langCode = '';      //for i18n
  public i18n : I18nService;
  public baseApi : ApiService;
  public topBar : TopBarService;
  public jslib = jslib;
  public router: Router;
  public popup:PopupService;
  public config = Config;
  public platform = '';
  constructor(  
   //public i18n : I18nService
  ) {
    //console.log('constructor BaseComponent');
    //for i18n
    this.i18n = AppModule.injector.get(I18nService);
    this.baseApi = AppModule.injector.get(ApiService);
    this.topBar = AppModule.injector.get(TopBarService);
    this.langCode = this.i18n.getLangCode();
    this.router = AppModule.injector.get(Router);
    this.i18n.eventUpdated.subscribe((lang)=>{
       this.langCode = lang;
    });
    this.popup = AppModule.injector.get(PopupService);
    if(this.config.CordovaMode){
      if((device.platform == "Android")){
        this.platform = 'android';
      }else{
        this.platform = 'ios';
      }
    }else{
      this.platform = 'android';
    }
    //User data sample
    // {
    //   "UserID": 1,
    //   "Token": "RmRqeXlXS3IvNnBqR0RNSjhJVHdPZz09",
    //   "Act": "youllook@gmail.com",
    //   "IdentityNo": "",
    //   "Birth": "1988/10/17",
    //   "Country": "14",
    //   "PhonePrefix": "+886",
    //   "Phone": "0982393454",
    //   "ImageUrl": "http://localhost:12005",
    //   "Prefer": [
    //     {
    //       "PType": "q1",
    //       "PAnswer": "1,3"
    //     },
    //     {
    //       "PType": "q2",
    //       "PAnswer": "0"
    //     },
    //     {
    //       "PType": "q3",
    //       "PAnswer": "1,2"
    //     },
    //     {
    //       "PType": "q4",
    //       "PAnswer": "1,3"
    //     },
    //     {
    //       "PType": "q5",
    //       "PAnswer": "2"
    //     },
    //     {
    //       "PType": "q6",
    //       "PAnswer": "1"
    //     }
    //   ]
    // }
  }
  IsLogin(){
    return (typeof this.GetUser() != 'undefined' && this.GetUser() != null);
  }
  GetUser(){
    return this.jslib.GetStorage('user');
  }
  //設定body class
  setBodyClass(value){
    document.body.classList = value;
  }
  //手動回上一頁機制
  backpage(){
    console.log('backpage');
    //若從home頁來又沒資料才轉向
    var his = this.jslib.GetStorage('history');
    his = (his) ? his : [];
    var url = '';
    if(his.length > 2){
      url = his[his.length-2];
    }
    if(url != ''){
      //clear record
      var h = his.slice(0,his.length-2);
      this.jslib.SetStorage('history',h);
      console.log('page Back');
      this.router.navigate([url]);
      if(url == '/'){
        this.router.navigate(['/layout/home']);
      }else{
        this.router.navigate([url]);
      }
    }else{
      this.router.navigate(['/layout/home']);
    }
    
    //window.history.back();
  }
}
