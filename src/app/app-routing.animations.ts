import {
  trigger,
  transition,
  style,
  query,
  group,
  animateChild,
  animate,
  keyframes
} from '@angular/animations';
//教學網址:https://www.youtube.com/watch?v=7JA90VI9fAI
//https://fireship.io/lessons/angular-router-animations/
export const transformer = 
trigger('routeAnimations',[
    transition('*<=>*',[
      query(':enter,:leave',[
        style({
          position:'absolute',
          width:'100vw',
          opacity:0,
          transform:'translateX({{offsetEnter}}%) translateY({{offsetTop}}%) scale(1)',
        }),
      ], {optional: true}),
      query(':enter',[
        animate('300ms ease',
        style({
          opacity:1,
          transform:'translateX(0)',
          position:'absolute'
        })
      ),
      ], {optional: true})
    ])
]);
// export const transformer = 
// trigger('routeAnimations', [
//   transition('* <=> *', [
//     group([
//       query(':enter', [
//         style({transform: 'translateX(100%)'}),
//         animate('0.4s ease-in-out', style({transform: 'translateX(0%)'}))
//       ], {optional: true}),
//       query(':leave', [
//         style({transform: 'translateX(0%)'}),
//         animate('0.4s ease-in-out', style({transform: 'translateX(-100%)'}))
//       ], {optional: true}),
//     ])
//   ]),
// ]);
// export const transformer =
//   trigger('routeAnimations', [
    
//     transition('* => isRight', transformTo({ x: -300, y: -100, rotate: 0 }) ),
//     transition('* => isLeft', transformTo({ x: 100, y: -100, rotate: 0 }) ),
//     transition('isRight => *', transformTo({ x: -300, y: -100, rotate: 0 }) ),
//     transition('isLeft => *', transformTo({ x: 100, y: -100, rotate: 0 }) ),
//     // transition('* => *', transformTo({ x: -300, y: -100, rotate: 90 }) )
// ]);


// function transformTo({x = 100, y = 0, rotate = 0}) {
//   const optional = { optional: true };
//   console.log('transformTo');
//   return [
//     query(':enter, :leave', [
//       style({
//         position: 'absolute',
//         top: 0,
//         left: 0,
//         width: '100vw'
//       })
//     ], optional),
//     query(':enter', [
//       style({ transform: `translate(${x}%, ${y}%) rotate(${rotate}deg)`},)
//     ]),
//     group([
//       query(':leave', [
//         animate('600ms ease-out', style({ transform: `translate(${x}%, ${y}%) rotate(${rotate}deg)`}))
//       ], optional),
//       query(':enter', [
//         animate('600ms ease-out', style({ transform: `translate(0, 0) rotate(0)`}))
//       ])
//     ]),
//   ];
// }
