import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'news-component',
  templateUrl: './news.component.html'
})
export class NewsComponent extends BaseComponent implements OnInit {
  newslist;
  newsfirst;
  constructor(
  ) {
    super();
    this.topBar.set({
      bg: '',
      header: 'mainbg',
      txt: 'A13.009',
      backfun: () => {
        this.backpage();
        //this.router.navigate(['/layout/find']);
      },
      tabShow: false
    });

    //取得官方消息
    this.baseApi.apiPost('News/GetNewsList',{}).then(
      (res:any) => {
          var list = res;
          if(list && list.length >= 1){
            this.newsfirst = list[0];
            this.newslist = list.slice(1);//移除第一個
         }else{
          this.newsfirst == null;
          this.newslist = [];
         }
      }
    )
  }

  ngOnInit() {

  }

}
