import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { NavigationEnd,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'newsDetail-component',
  templateUrl: './newsDetail.component.html'
})
export class NewsDetailComponent extends BaseComponent implements OnInit {
  id;
  newsData;
  subscribe;
  constructor(
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg: '',
      header: '',
      txt: 'A13.009',
      backfun: () => {
        this.backpage();
        //this.router.navigate(['/layout/news']);
      },
      tabShow: false
    });
  }
  ngOnInit() {
    //切頁畫面重置 若放constructor api會打兩次
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.GetNewsData(this.route.snapshot.paramMap.get('id'));
    });
    this.GetNewsData(this.route.snapshot.paramMap.get('id'));
  }

  public GetNewsData(id) {
    this.id = id;
    this.baseApi.apiPost('news/GetNewsData', { id: this.id}).then(
      (res) => {
        this.newsData = res;
      }
    )
  }

  After(id){
    this.router.navigate(['/layout/news/detail/' + id]);
  }

  ShareEvt(){
  this.topBar.OpenShare('type=news&id=' + this.newsData.ID);
  }
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }
}
