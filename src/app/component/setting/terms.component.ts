import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'terms-component',
  templateUrl: './terms.component.html'
})
export class TermsComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'服务条款和个资声明',
      backfun:()=>{
        this.backpage();
      },
      tabShow: false
    });
  }
  ngOnInit() {

  }
  
}
