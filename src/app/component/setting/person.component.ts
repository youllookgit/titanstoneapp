import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'person-component',
  templateUrl: './person.component.html'
})
export class PersonComponent extends BaseComponent implements OnInit {

  _User;
  Countries;
  public mainForm: FormGroup;
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E11.004',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false
    });
    this._User = this.GetUser();
    //取得國家
    this.baseApi.apiPost('Option/GetOptions',{code:'Country'}).then(
      (res)=>{
         this.Countries = res;
      }
    )   
  }
  
  ngOnInit() {
    var birth = this._User['Birth'];
    birth = this.jslib.replaceAll(birth,'/','-');
    // 建立表單規格內容
    this.mainForm = new FormGroup({
      UserID: new FormControl(this._User['UserID'], [Validators.required]),
      IdentityNo: new FormControl(this._User['IdentityNo'], [Validators.required]),
      Country: new FormControl(this._User['Country'], [Validators.required]),
      Birth: new FormControl(birth, [Validators.required]),
      PhonePrefix: new FormControl(this._User['PhonePrefix'], [Validators.required,Validators.maxLength(5),Validators.minLength(2)]),
      Phone: new FormControl(this._User['Phone'], [Validators.required,Validators.maxLength(12),Validators.minLength(8)])
    });
  }
  Prefix(){
    var c = this.mainForm.value['Country'];
    if(c != ''){
      var citem = this.Countries.find((item)=>{
        return item['value'] == c;
      });
      if(citem){
        this.mainForm.get('PhonePrefix').setValue('+' + citem['extend1']);
        this.mainForm.updateValueAndValidity();
      }
    }
  }
  FormSubmit(){
    var _user = this.jslib.GetStorage('user');
    var _form = this.mainForm.value;
    var newData = Object.assign(_user,_form);
    //Submit 
    this.baseApi.apiPost('User/UpdateUser',newData).then((res)=>{
      this.jslib.SetStorage('user',_user);
      //顯示提示訊息
      this.popup.setConfirm({
        content:this.i18n.Trans('E51.036'),
        event:()=>{
          this.router.navigate(['layout/set']);
        }
      });
    },(err)=>{
      this.popup.setConfirm(err);
    });
  }
  DateCheck(){
    var d = this.mainForm.value['Birth'];
    var _d = new Date(d);
    var today = new Date();
    if(_d.getTime() > (today.getTime() + (24*3600*1000))){
      this.popup.setConfirm({
        content:this.i18n.Trans('A17.016'),
        event:()=>{
          this.mainForm.get('Birth').setValue('');
          this.mainForm.updateValueAndValidity();
        }
      });
    }
  }
}
