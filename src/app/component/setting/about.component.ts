import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'about-component',
  templateUrl: './about.component.html'
})
export class AboutComponent extends BaseComponent implements OnInit  {

  lang = '';
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E11.003',
      backfun:()=>{
        this.backpage();
      },
      tabShow: false
    });
  }
  ngOnInit() {
    this.lang = this.i18n.getLangCode();

  }
}
