import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var device;
@Component({
  selector: 'lang-component',
  templateUrl: './lang.component.html'
})
export class LangComponent extends BaseComponent implements OnInit {

  
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E91.001',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false
    });
  }

  ngOnInit() {
  
  }
  setLang(lang){
    this.i18n.setLang(lang);
    this.langCode = lang;
    var _pushid = this.jslib.GetStorage('pushid');
    if(_pushid){
      var _notice = {
        DeviceID:device.uuid,
        Platform:device.platform,
        Version:device.version,
        PushID:_pushid,
        Lang:this.langCode
      };
      this.baseApi.backPost('User/RegistNotice',_notice).then((res)=>{
        this.jslib.SetStorage('notice',res);
      });
    }
    
  }
  
}
