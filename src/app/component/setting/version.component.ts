import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { Config } from '../../../assets/configuration/config';
declare var device;
declare var window
@Component({
  selector: 'version-component',
  templateUrl: './version.component.html'
})
export class VersionComponent extends BaseComponent implements OnInit {
  public _version = '';
  public deviceInfo = '';
  public uuid = '';
  public pushid = '';
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E81.001',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false
    });
    this._version = Config.VERSION;

    if(Config.CordovaMode){
      this.deviceInfo = device.platform + ' ' + device.version;
      this.uuid = device.uuid;
      this.pushid = this.jslib.GetStorage('pushid');
    }
  }

  
  ngOnInit() {
  
  }

  
}
