import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'setting-component',
  templateUrl: './setting.component.html'
})
export class SettingComponent extends BaseComponent implements OnInit {
  fileUrl = 'assets/img/pdefault.png';
  menuHidden = false;
  UserData;
  constructor(  
  ) {
    super();
    this.topBar.setBgClass('menebg');
    if(this.IsLogin()){
      this.UserData = this.GetUser();
      if(this.UserData['ImageUrl'] != ''){
        this.fileUrl = this.UserData['ImageUrl'];
      }
      this.menuHidden = true;
    }
  }

  ngOnInit() {
   
  }
  UploadHandler() {
    console.log('UploadHandler');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','image/png, image/gif, image/jpeg, image/bmp');
  
    Fileinput.addEventListener('change', () =>  {
        if (Fileinput.files != null && Fileinput.files[0] != null) {
          //驗大小
          console.log('Imageinput.files',Fileinput.files[0]);
          this.baseApi.apiPostFile('User/ImgUpd',Fileinput.files[0]).then((url:any)=>{
            console.log('Upload res',url);
            this.fileUrl = url;
            this.SaveUserImg(this.fileUrl);
          },(err)=>{
            this.popup.setConfirm({content:err['msg']});
          });
        }
    });
    Fileinput.click();
  }
  Logout(){
    this.jslib.SetStorage('user',null);
    this.router.navigate(['layout/login']);
  }
  SaveUserImg(file){
      if(file && file != ''){
        this.UserData['ImageUrl'] = file;
        this.jslib.SetStorage('user',this.UserData);
        this.baseApi.backPost('User/SaveImg',{img:file}).then((res)=>{
          console.log('SaveImg',res);
        });
      }
  }
}
