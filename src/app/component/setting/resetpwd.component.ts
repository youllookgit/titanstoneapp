import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'resetpwd-component',
  templateUrl: './resetpwd.component.html'
})
export class ResetpwdComponent extends BaseComponent implements OnInit {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E41.001',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false
    });
  }
  public mainForm: FormGroup;
  ngOnInit() {
      // 建立表單規格內容
      this.mainForm = new FormGroup({
        OldPwd: new FormControl('', [Validators.required,Validators.minLength(6)]),
        NewPwd: new FormControl('', [Validators.required,Validators.minLength(6)]),
        PwdCheck: new FormControl('', [Validators.required,Validators.minLength(6)])
      });
  }
  FormSubmit(){
    var _form = this.mainForm.value;
    if(_form['NewPwd'] != _form['PwdCheck']){
      this.popup.setConfirm({
        content:this.i18n.Trans('A114.009')
      });
    }else{
          //Submit 
    this.baseApi.apiPost('User/UpdatePwd',this.mainForm.value).then(
      (res)=>{
        console.log('UpdatePwd',res);
          //顯示提示訊息
          this.popup.setConfirm({
              content:this.i18n.Trans('E41.011'),
              event:()=>{
                this.jslib.SetStorage('user',null);
                this.router.navigate(['layout/login']);
              }
          });
        },(err)=>{
          this.popup.setConfirm(err['msg']);
        });
    }
  }
  
}
