import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
declare var device;
@Component({
  selector: 'notice-component',
  templateUrl: './notice.component.html'
})
export class NoticeComponent extends BaseComponent implements OnInit {
  allset = true;
  itemHidden = this.IsLogin();
  notice;
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E61.001',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false
    });
    this.notice = this.jslib.GetStorage('notice');
    console.log('notice',this.notice);
    //沒取到PushID再取一次
    if(typeof this.notice == 'undefined'){
      var _notice = {
        DeviceID:'offline',
        Platform:'PC',
        Version:'1.0.0',
        PushID:'no-id',
        Lang:''
      };
      if(this.config.CordovaMode){
        _notice.DeviceID = device.uuid;;
        _notice.Platform = device.platform;
        _notice.Version = device.version;
        _notice.PushID = this.jslib.GetStorage('pushid');
        _notice.Lang = this.jslib.GetStorage('lang');
      }
      console.log('notice.PushID',_notice);
      if(_notice.PushID){
        this.baseApi.backPost('User/RegistNotice',_notice).then((res)=>{
          this.jslib.SetStorage('notice',res);
          this.notice = res;
          this.InitNotice();
         });
      }
    }else{
      this.InitNotice();
    }

  }
  public mainForm: FormGroup;
  ngOnInit() {

  }
  InitNotice(){
    this.mainForm = new FormGroup({
      Pay: new FormControl(this.notice['Pay']),
      Rent: new FormControl(this.notice['Rent']),
      Estate:new FormControl(this.notice['Estate']),
      Activity:new FormControl(this.notice['Activity']),
      Trend:new FormControl(this.notice['Trend']),
      News:new FormControl(this.notice['News']),
      System:new FormControl(this.notice['System'])
    });
    this.Checkall();
  }
  Setall(){
    var set = this.allset;
    this.mainForm = new FormGroup({
      Pay: new FormControl(set),
      Rent: new FormControl(set),
      Estate:new FormControl(set),
      Activity:new FormControl(set),
      Trend:new FormControl(set),
      News:new FormControl(set),
      System:new FormControl(set)
    });
    this.mainForm.updateValueAndValidity();
    this.Update();
    console.log('');
  }
  Checkall(){
    var _check = false;
    if(this.IsLogin()){
      _check = (
        this.mainForm.value['Pay'] ||
        this.mainForm.value['Rent'] ||
        this.mainForm.value['Estate'] ||
        this.mainForm.value['Activity'] ||
        this.mainForm.value['Trend'] ||
        this.mainForm.value['News'] ||
        this.mainForm.value['System']
      );
      this.allset = _check;
    }else{
      _check = (
        this.mainForm.value['Estate'] ||
        this.mainForm.value['Activity'] ||
        this.mainForm.value['Trend'] ||
        this.mainForm.value['News'] ||
        this.mainForm.value['System']
      );
      this.allset = _check;
    }
    this.Update();
  }
  Update(){
     Object.assign(this.notice,this.mainForm.value);
     this.baseApi.backPost('User/NoticeUpd',this.notice);
     this.jslib.SetStorage('notice',this.notice);
  }

  
}
