import { Component, OnInit,NgZone } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
@Component({
  selector: 'prefer-component',
  templateUrl: './prefer.component.html'
})
export class PreferComponent extends BaseComponent implements OnInit {
  PreferData;
  constructor(  
    public ngzone : NgZone
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E51.001',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false
    });
    this.PreferData = this.GetUser()['Prefer'];
  }
  
  ngOnInit() {
    
    if(this.PreferData){
      this.ngzone.runOutsideAngular(()=>{
        this.PreferData.forEach(item => {
           var _val = item['PAnswer'].split(',');
           $('input[name="' + item['PType'] + '"]').val(_val);
        });
      });
    }
  }
  FormSubmit(){
    var prefer =  $('#prefer').serializeObject();
    var _pdata = [];
    for(var i = 1;i <= 7 ;i++){
       var result = prefer[('q' + i)];
       result = (result) ? result : '';
       result = (typeof result == 'object') ? result.join(',')  : result;
       var answer = {
        PType:('q' + i),
        PAnswer:result
       };
       _pdata.push(answer);
    }
    console.log('_pdata',_pdata);
    this.baseApi.apiPost('User/UpdatePrefer',_pdata).then((res)=>{
      //回存User data
      var _user = this.jslib.GetStorage('user');
      _user['Prefer'] = _pdata;
      this.jslib.SetStorage('user',_user);
      //顯示提示訊息
      this.popup.setConfirm({
        content:this.i18n.Trans('E51.036'),
        event:()=>{
          this.router.navigate(['layout/set']);
        }
      });
    },(err)=>{
      this.popup.setConfirm(err);
    });
  }
  
}
