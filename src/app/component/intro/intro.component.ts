import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var Swiper;
@Component({
  selector: 'intro-component',
  templateUrl: './intro.component.html'
})
export class IntroComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }
public swipe;
ngOnInit() {
  var txt = [
    {h1:this.i18n.Trans('A11.001'),desc:this.i18n.Trans('A11.002')},
    {h1:this.i18n.Trans('A11.003'),desc:this.i18n.Trans('A11.004')},
    {h1:this.i18n.Trans('A11.005'),desc:this.i18n.Trans('A11.006')},
    {h1:this.i18n.Trans('A11.007'),desc:this.i18n.Trans('A11.008')}
  ];
  setTimeout(()=>{
    this.swipe = new Swiper('#init', {
      autoplay: {
        delay: 3000,
      },
      spaceBetween:0,
      pagination: {
        el: '.init-pagination',
      }
    });
    this.swipe.on('slideChange',  () => {
        var _msg = txt[this.swipe.realIndex];
        $('#init_title').text(_msg.h1);
        $('#init_msg').text(_msg.desc);
    });
  },250);
}
init(){
  console.log('Init');
  this.router.navigate(['layout/login']);
}
  
}
