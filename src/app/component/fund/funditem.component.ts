import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { NavigationEnd,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'funditem-component',
  templateUrl: './funditem.component.html'
})
export class FunditemComponent extends BaseComponent implements OnInit  {

  FID;
  FData;
  FundType;
  BonusRate;
  constructor(
    public route : ActivatedRoute
  ) {
    super();

    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B31.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });

    this.FData = this.jslib.GetStorage('fund');
    this.FundType = this.FData['FundType'];
    if(this.FData.Profit > 0 && this.FData.Cost > 0){
      this.BonusRate = ((this.FData.Profit/this.FData.Cost)*100);
    }else{
      this.BonusRate = 0;
    }

    console.log('FData',this.FData)
  }
  subscribe;
  Data;
  ngOnInit() {
    //切頁畫面重置 若放constructor api會打兩次
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.GetData(this.route.snapshot.paramMap.get('id'));
    });
    this.GetData(this.route.snapshot.paramMap.get('id'));
  }


  Record; //交易明細原始資料
  FilterRecord;//交易明細篩選資料
  TypeOpt;
  TypeSelect = '';
  ChartList;
  ChartData;
  GetData(_id){
    this.FID = _id;
    console.log('FDetail',_id)
    this.baseApi.apiPost('User/MyFundData', { id: _id}).then(
      (res) => {
        this.Data = res;
        console.log('MyFundData',this.Data);
        this.TypeOpt = res['RecordOpt'];
        if(res['TransRecord']){
          this.Record = res['TransRecord'];
          this.FilterRecord = res['TransRecord'];
        }else{
          this.Record = [];
          this.FilterRecord = [];
        }
        if(res['FundWorth'] && res['FundWorth'].length > 0){
          this.ChartList = res['FundWorth'];
          this.SetChart(res['FundWorth'][0]);
        }
      }
    )
  }
  Filter(){
    console.log('TypeSelect',this.TypeSelect);
    if(this.TypeSelect == ''){
      this.FilterRecord = this.Record;
    }else{
      var filter = this.Record.filter(r => r['TxnType'] == this.TypeSelect);
      this.FilterRecord  = filter;
    }
    console.log('FilterRecord',this.FilterRecord);
  }
  SetChart(setData){
    this.ChartList.forEach(cdata => {
      if(cdata['Year'] == setData['Year']){
        cdata['active'] = true;
      }else{
        cdata['active'] = false;
      }
    });
    this.ChartData = setData;
  };
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }
  Report(){
    this.jslib.SetStorage('report','B31.052');
    this.router.navigate(['/layout/myfundhis/' + this.FID]);
  }
}
