import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { NavigationEnd,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'fundhis-component',
  templateUrl: './fundhis.component.html'
})
export class FundHisComponent extends BaseComponent implements OnInit  {

  constructor(
    public route : ActivatedRoute
  ) {
    super();
    var titleCode = this.jslib.GetStorage('report');
    if(titleCode != 'B31.052'){
      titleCode = 'B31.051';
    }

    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B31.051',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  subscribe;
  Data;
  ngOnInit() {
    //切頁畫面重置 若放constructor api會打兩次
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.GetData(this.route.snapshot.paramMap.get('id'));
    });
    this.GetData(this.route.snapshot.paramMap.get('id'));
  }
  GetData(_id){
    this.baseApi.apiPost('User/MyFundReport', { id: _id}).then(
      (res) => {
        this.Data = res;
      }
    )
  }
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }

}
