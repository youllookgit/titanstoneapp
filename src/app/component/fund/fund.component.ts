import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var Swiper;
@Component({
  selector: 'fund-component',
  templateUrl: './fund.component.html'
})
export class FundComponent extends BaseComponent implements OnInit  {

  constructor(
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B31.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  public CostSum = 0;
  public FData;
  ngOnInit() {
    this.baseApi.apiPost('User/MyFund',{}).then(
      (res:any)=>{
          console.log('res',res);
          if(res && res.length > 0){
            this.FData = res;
            res.forEach(item => {
              this.CostSum += item['Cost'];
            });
          }else{
            this.FData = [];
          }

      }
    )
  }
  ToDetail(item){
    this.jslib.SetStorage('fund',item);
    this.router.navigate(['/layout/myfunditem/' + item.ID]);
  }
}
