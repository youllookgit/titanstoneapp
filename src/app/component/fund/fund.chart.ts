import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
declare var $;
declare var Chart;

@Component({
  selector: 'fund-chart',
  templateUrl: './fund.chart.html'
})
export class FundChartComponent implements OnInit  {
  @Input() SourceData;
  
  constructor(  
  ) {
  }
  public chart;
  public Data;

  VerticalColor = '#DFDFDF'; //縱線顏色
  LastMonthIndex = 0; //縱線位置
  AddPoint = 0; //差異數量
  DiffPercent = 0; //差異百分比
  UpperClase = '';
  ngOnInit() {

    // For Test
    // var _label = ['01','','','','','06','','','','','','12']
    // var _data = [12,13,15.5,17,14,18,21.8,null,null,null,null]

    // for (let index = 0; index < _data.length; index++) {
    //   if(_data[index] != null){
    //      this.LastMonthIndex = index;
    //   }
    // }
    // if(_data.length >= 2){
    //   var n1 = _data[this.LastMonthIndex];
    //   var n2Index = 0;
    //   for (let index = 0; index < _data.length; index++) {
    //     if(_data[index] != null && index != this.LastMonthIndex){
    //       n2Index = index;
    //     }
    //   }
    //   var n2 = _data[n2Index];
    //   this.AddPoint = Number((n1 - n2).toFixed(2));
    //   this.DiffPercent = Number(((this.AddPoint/n2)*100).toFixed(2));
    //   if(this.AddPoint > 0){
    //     this.UpperClase = 'upper';
    //     this.VerticalColor = '#FF8471'
    //   }else{
    //     this.UpperClase = 'down';
    //     this.VerticalColor = '#4FBE1D'
    //   }
    // }
    
    // this.chart = null; 
    // setTimeout(()=>{
    //          var vColor = this.VerticalColor;
    //         //Extend
    //         const verticalLinePlugin = {
    //           getLinePosition: function (chart, pointIndex) {
    //               const meta = chart.getDatasetMeta(0); // first dataset is used to discover X coordinate of a point
    //               const data = meta.data;
    //               return data[pointIndex]._model.x;
    //           },
    //           renderVerticalLine: function (chartInstance, pointIndex) {
    //               const lineLeftOffset = this.getLinePosition(chartInstance, pointIndex);
    //               const scale = chartInstance.scales['y-axis-0'];
    //               const context = chartInstance.chart.ctx;
            
    //               // render vertical line
    //               context.beginPath();
    //               context.strokeStyle = vColor; //'#FF8471';
    //               context.moveTo(lineLeftOffset, scale.top);
    //               context.lineTo(lineLeftOffset, scale.bottom);
    //               context.stroke();
    //               // write label
    //               //context.fillStyle = "#ff0000";
    //               //context.textAlign = 'center';
    //               //context.fillText('MY TEXT', lineLeftOffset, (scale.bottom - scale.top) / 2 + scale.top);
    //           },
            
    //           afterDatasetsDraw: function (chart, easing) {
    //               if (chart.config.lineAtIndex) {
    //                   chart.config.lineAtIndex.forEach(pointIndex => this.renderVerticalLine(chart, pointIndex));
    //               }
    //           }
    //           };
    //           Chart.plugins.register(verticalLinePlugin);

    //   var canvas:any;
    //   canvas = document.getElementById("Lchart");
    //   var ctx = canvas.getContext("2d");

    //   var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
    //       gradientStroke.addColorStop(0, '#FFE396');
    //       gradientStroke.addColorStop(1, '#FACE34');

    //   var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
    //       gradientFill.addColorStop(0, '#F5BE01');
    //       gradientFill.addColorStop(1, '#FFEFC4');

    //   var Setdata = {
    //           labels: _label,
    //           datasets: [{
    //             borderColor: gradientStroke,
    //             pointBorderColor: gradientStroke,
    //             pointBackgroundColor: gradientStroke,
    //             pointHoverBackgroundColor: gradientStroke,
    //             pointHoverBorderColor: gradientStroke,
    //             pointRadius: 0,
    //             fill: true,
    //             backgroundColor: gradientFill,
    //             borderWidth: 1,
    //             lineTension: 0,
    //               data: _data
    //           }]
    //       };
    //   this.chart = new Chart(ctx, {
    //       type: 'line',
    //       data: Setdata,
    //       options:{
    //         legend: {
    //           display: false,
    //         },
    //         scales: {
    //           xAxes: [{
    //                   gridLines: {
    //                     display: false,
    //                   },
    //                   ticks: {
    //                     fontSize:10,
    //                     fontColor: '#cccccc',
    //                     fontFamily:'PingFang SC'
    //                   }
    //               }],
    //           yAxes: [{
    //                   gridLines: {
    //                     drawBorder: true,
    //                   },
    //                   ticks: {
    //                       fontSize:11,
    //                       fontColor: '#cccccc',
    //                       beginAtZero: true,
    //                       padding: 8,
    //                       // max: 6000
    //                   }
    //               }]
    //          },
    //       },
    //       lineAtIndex: [this.LastMonthIndex],
    //     });
    // },150);
  }
  ngOnChanges() {
    console.log('this.SourceData',this.SourceData);

    var _label = this.SourceData['xAxes'];//['01','','','','','06','','','','','','12']
    var _data = this.SourceData['yAxes'];//[12,13,15.5,17,14,18,21.8,null,null,null,null]

    for (let index = 0; index < _data.length; index++) {
      if(_data[index] != null){
         this.LastMonthIndex = index;
      }
    }
    if(_data.length >= 2){
      var n1 = _data[this.LastMonthIndex];
      var n2Index = 0;
      for (let index = 0; index < _data.length; index++) {
        if(_data[index] != null && index != this.LastMonthIndex){
          n2Index = index;
        }
      }
      var n2 = _data[n2Index];
      this.AddPoint = Number((n1 - n2).toFixed(2));
      this.DiffPercent = Number(((this.AddPoint/n2)*100).toFixed(2));
      if(this.AddPoint > 0){
        this.UpperClase = 'upper';
        this.VerticalColor = '#FF8471'
      }else{
        this.UpperClase = 'down';
        this.VerticalColor = '#4FBE1D'
      }
    }
    console.log('LastMonthIndex',this.LastMonthIndex);
    this.chart = null; 
    setTimeout(()=>{
             var vColor = this.VerticalColor;
            //Extend
            const verticalLinePlugin = {
              getLinePosition: function (chart, pointIndex) {
                  const meta = chart.getDatasetMeta(0); // first dataset is used to discover X coordinate of a point
                  const data = meta.data;
                  return data[pointIndex]._model.x;
              },
              renderVerticalLine: function (chartInstance, pointIndex) {
                  const lineLeftOffset = this.getLinePosition(chartInstance, pointIndex);
                  const scale = chartInstance.scales['y-axis-0'];
                  const context = chartInstance.chart.ctx;
            
                  // render vertical line
                  context.beginPath();
                  context.strokeStyle = vColor; //'#FF8471';
                  context.moveTo(lineLeftOffset, scale.top);
                  context.lineTo(lineLeftOffset, scale.bottom);
                  context.stroke();
                  // write label
                  //context.fillStyle = "#ff0000";
                  //context.textAlign = 'center';
                  //context.fillText('MY TEXT', lineLeftOffset, (scale.bottom - scale.top) / 2 + scale.top);
              },
            
              afterDatasetsDraw: function (chart, easing) {
                  if (chart.config.lineAtIndex) {
                      chart.config.lineAtIndex.forEach(pointIndex => this.renderVerticalLine(chart, pointIndex));
                  }
              }
              };
              Chart.plugins.register(verticalLinePlugin);

      var canvas:any;
      canvas = document.getElementById("Lchart");
      var ctx = canvas.getContext("2d");

      var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
          gradientStroke.addColorStop(0, '#FFE396');
          gradientStroke.addColorStop(1, '#FACE34');

      var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
          gradientFill.addColorStop(0, '#F5BE01');
          gradientFill.addColorStop(1, '#FFEFC4');

      var Setdata = {
              labels: _label,
              datasets: [{
                borderColor: gradientStroke,
                pointBorderColor: gradientStroke,
                pointBackgroundColor: gradientStroke,
                pointHoverBackgroundColor: gradientStroke,
                pointHoverBorderColor: gradientStroke,
                pointRadius: 0,
                fill: true,
                backgroundColor: gradientFill,
                borderWidth: 1,
                lineTension: 0,
                  data: _data
              }]
          };
      this.chart = new Chart(ctx, {
          type: 'line',
          data: Setdata,
          options:{
            legend: {
              display: false,
            },
            scales: {
              xAxes: [{
                      gridLines: {
                        display: false,
                      },
                      ticks: {
                        fontSize:10,
                        fontColor: '#cccccc',
                        fontFamily:'PingFang SC'
                      }
                  }],
              yAxes: [{
                      gridLines: {
                        drawBorder: true,
                      },
                      ticks: {
                          fontSize:11,
                          fontColor: '#cccccc',
                          beginAtZero: true,
                          padding: 8,
                          // max: 6000
                      }
                  }]
             },
          },
          lineAtIndex: [this.LastMonthIndex],
        });
    },150);
  }

  ngOnDestroy(){
    this.chart = null; 
  }
}