import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { Router} from '@angular/router';
declare var $;
declare var Swiper;

@Component({
  selector: 'estate-slider',
  templateUrl: './estate.slider.html'
})
export class EstateSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
    public ngzone : NgZone,
    public router: Router
  ) {
  }
  public swiper2;
  public Data;
  ngOnInit() {
  }
  ngOnChanges() {
    this.swiper2 = null; 
    setTimeout(()=>{
      if(this.swiper2 == null && this.SourceData){
        this.swiper2 = new Swiper('#estate', {
          // spaceBetween: 10,
          effect: 'coverflow',
          grabCursor: true,
          centeredSlides: true,
          slidesPerView: 'auto',
          coverflowEffect: {
              rotate: 30,
              stretch: 0,
              depth: 60,
              modifier: 1,
              slideShadows : true,
          },
          pagination: {
            el: '.home-pagination',
          }
        });
      }
      //   this.swiper2.on('slideChange',  () => {
      //     console.log('slideChange',this.swiper2.activeIndex);
      //  });

    },250);
  }
  ngOnDestroy(){
    this.swiper2 = null; 
  }
  RouteTo(id){
    this.router.navigate(['/layout/estate/item/' + id]);
  }
}
