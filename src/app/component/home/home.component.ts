import { Component, OnInit ,NgZone } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var Swiper;

@Component({
  selector: 'home-component',
  templateUrl: './home.component.html'
})
export class HomeComponent extends BaseComponent implements OnInit  {

  constructor(  
    public ngzone : NgZone,
  ) {
    super();
    this.topBar.set({
      bg:'bg_gray',
      header:'',
      txt:'Titan Stone'
    });
  }

  public mainSlider;
  public trendSlider;
  public seminarSlider;
  public newsData;
  ngOnInit() {
    this.baseApi.apiPost('Home/Main',{}).then((res)=>{
      console.log('res',res);
      this.mainSlider = res['Estate'];
      this.trendSlider = res['Trend'];
      this.seminarSlider = res['Act'];
      this.newsData = res['News'];
    },(err)=>{
      console.log('err',err);
    })
    //this.popup.setLoading(true);
    // setTimeout(()=>{
 
    //   this.mainSlider = [
    //     {
    //       id:1,
    //       img:'assets/img/temp/product1.png',
    //       title:'KY iCenter 智能云端商办',
    //       name:'柬埔寨 / Cambodia'
    //     },
    //     {
    //       id:2,
    //       img:'assets/img/temp/product2.png',
    //       title:'KY iCenter 智能云端商办',
    //       name:'柬埔寨 / Cambodia'
    //     },
    //     {
    //       id:3,
    //       img:'assets/img/temp/product3.png',
    //       title:'KY iCenter 智能云端商办',
    //       name:'柬埔寨 / Cambodia'
    //     }
    //   ];
    //   this.trendSlider = [
    //     {
    //       id:1,
    //       img:'assets/img/temp/around8.png',
    //       title:'柬埔寨计划5年投资37亿美元修建6机场'
    //     },
    //     {
    //       id:2,
    //       img:'assets/img/temp/around8.png',
    //       title:'柬埔寨计划5年投资37亿美元修建6机场'
    //     },
    //     {
    //       id:3,
    //       img:'assets/img/temp/around8.png',
    //       title:'柬埔寨计划5年投资37亿美元修建6机场'
    //     }
    //   ];
    //   this.seminarSlider = [
    //     {
    //       id:1,
    //       img:'assets/img/temp/around8.png',
    //       tag:'活动',
    //       title:'柬埔寨计划5年投资37亿美元修建6机场',
    //       date:'2019/06/29 (六)'
    //     },
    //     {
    //       id:2,
    //       img:'assets/img/temp/around8.png',
    //       tag:'活动',
    //       title:'柬埔寨计划5年投资37亿美元修建6机场',
    //       date:'2019/06/29 (六)'
    //     },
    //     {
    //       id:3,
    //       img:'assets/img/temp/around8.png',
    //       tag:'活动',
    //       title:'柬埔寨计划5年投资37亿美元修建6机场',
    //       date:'2019/06/29 (六)'
    //     }
    //   ];
    //   this.newsData = [
    //     {
    //       id:1,
    //       img:'assets/img/temp/trd2.png',
    //       title:'出来借的总要还 2021年中国地产商偿债规模上看1兆人民币',
    //       date:'2019/6/24'
    //     },
    //     {
    //       id:2,
    //       img:'assets/img/temp/trd2.png',
    //       title:'出来借的总要还 2021年中国地产商偿债规模上看1兆人民币',
    //       date:'2019/6/24'
    //     },
    //     {
    //       id:3,
    //       img:'assets/img/temp/trd2.png',
    //       title:'出来借的总要还 2021年中国地产商偿债规模上看1兆人民币',
    //       date:'2019/6/24'
    //     }
    //   ];
      
    //   this.popup.setLoading(false);

    // },250);

    // this.ngzone.runOutsideAngular(function(){
      

    //   var swiperFloor = new Swiper('#floor', {
    //     spaceBetween: 10,
    //     effect: 'coverflow',
    //       grabCursor: true,
    //       centeredSlides: true,
    //       slidesPerView: 'auto',
    //       coverflowEffect: {
    //           rotate: 30,
    //           stretch: 0,
    //           depth: 60,
    //           modifier: 1,
    //           slideShadows : true,
    //     },
    //     pagination: {
    //       el: '.home-pagination',
    //     }
    //   });

    // });
    
  }
  
}
