import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
declare var $;
declare var Swiper;

@Component({
  selector: 'seminar-slider',
  templateUrl: './seminar.slider.html'
})
export class SeminarSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
  ) {
  }
  public swiper;
  public Data;
  ngOnInit() {
 
    
  }
  ngOnChanges() {
    this.swiper = null; 

    setTimeout(()=>{
       this.swiper = new Swiper('#actcmd', {
          spaceBetween: 15
        });
    },250);
  }
  ngOnDestroy(){
    this.swiper = null; 
  }
}
