import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
declare var $;
declare var Swiper;

@Component({
  selector: 'trend-slider',
  templateUrl: './trend.slider.html'
})
export class TrendSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
  ) {
  }
  public swiper;
  public Data;
  ngOnInit() {
  }
  ngOnChanges() {
    this.swiper = null; 
    setTimeout(()=>{
      this.swiper = new Swiper('#trendcmd', {
          spaceBetween: 15
        });
    },250);
  }
  ngOnDestroy(){
    this.swiper = null; 
  }
}
