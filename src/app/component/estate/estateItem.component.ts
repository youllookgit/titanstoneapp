import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { ActivatedRoute } from '@angular/router';
import { calcPossibleSecurityContexts } from '@angular/compiler/src/template_parser/binding_parser';
declare var startApp;
declare var Swiper;
@Component({
  selector: 'estateItem-component',
  templateUrl: './estateItem.component.html'
})
export class EtateItemComponent extends BaseComponent implements OnInit  {

  public Data;
  public _id;
  constructor(  
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'',
      txt:'',
      backfun:()=>{
        this.router.navigate(['/layout/estate']);
      },
      tabShow:false
    });
    var index = this.route.snapshot.paramMap.get('id');
    this._id = index;
    this.GetData(index);
  }
  public floorData;
  public deviceData;
  public introData;
  
  ngOnInit() {
    // this.floorData = [
    //   {
    //     img:'assets/img/temp/floor.jpg',
    //     title:'景觀戶型圖',
    //     desc:'投報高租金坪效最佳',
    //     floor:'2-18F',
    //     stand:'75㎡~147㎡',
    //   },
    //   {
    //     img:'assets/img/temp/floor.jpg',
    //     title:'景觀戶型圖',
    //     desc:'投報高租金坪效最佳',
    //     floor:'2-18F',
    //     stand:'75㎡~147㎡',
    //   },
    //   {
    //     img:'assets/img/temp/floor.jpg',
    //     title:'景觀戶型圖',
    //     desc:'投報高租金坪效最佳',
    //     floor:'2-18F',
    //     stand:'75㎡~147㎡',
    //   }
    // ];
    // this.deviceData = [
    //   {
    //     img:'assets/img/temp/around8.png'
    //   },
    //   {
    //     img:'assets/img/temp/around8.png'
    //   },
    //   {
    //     img:'assets/img/temp/around8.png'
    //   }
    // ];
    // this.introData = [
    //   {
    //     img:'assets/img/temp/sport1.jpg'
    //   },
    //   {
    //     img:'assets/img/temp/sport2.jpg'
    //   },
    //   {
    //     img:'assets/img/temp/sport3.jpg'
    //   }
    // ];
  }
  Device;
  DeviceList;
  AroundList;
  Album;
  ServiceList;
  actlist;
  FrameUrl = '';
  VideoImg = '';
  //FrameHeight;

  GetData(_id){
    this.baseApi.apiPost('Estate/GetDetail',{ id: _id}).then(
      (res) => {
        this.Data = res;

        //video
        if(this.Data['VideoUrl'] && this.Data['VideoUrl'] != ''){
          this.FrameUrl = this.Data['VideoUrl'];
          this.VideoImg = this.Data['VideoImg'];
        }
        //Album
        if(this.Data['Album']){
          this.Album = JSON.parse(this.Data['Album']);
        }

        this.deviceData = [];
        if(this.Data['Device']){
          this.Data['Device'] = JSON.parse(this.Data['Device']);
          this.Device = this.Data['Device'];

          if(this.Device['album']){
            this.Device['album'].forEach(img => {
              this.deviceData.push({
                img:img
              });
            });
          }
          this.DeviceList = this.Device['item'];
        }
        
        if(this.Data['Service']){
          var _s = JSON.parse(this.Data['Service']);
          this.ServiceList = _s['item'];
        }

        if(this.Data['Around']){
          var around = JSON.parse(this.Data['Around']);
          this.introData = [];
          around['album'].forEach(img => {
            this.introData.push({
              img:img
            });
          });
          this.AroundList = around['item'];
        }
        
        this.floorData = res['floor'];
        this.actlist = res['actlist'];

        //資料取回來再執行
        setTimeout(()=>{

          var swiperdevice = new Swiper('#device', {
            autoHeight: true,
            pagination: {
              el: '.device',
            }
          });
          //console.log('swiperdevice',swiperdevice);
          // if(this.Data['VideoUrl'] && this.Data['VideoUrl'] != ''){
          //   //this.FrameHeight = swiperdevice.height;
          //   this.FrameUrl = this.Data['VideoUrl'];
          //   this.VideoImg = this.Data['VideoImg'];
          // }
        },250);
      }
    )
  }
  public _tab = 1;
  tabSet(set){
       this._tab = set;
  }
  contact(){
    this.router.navigate(['/layout/contactus']);
  }
  ShareEvt(){
    this.topBar.OpenShare('type=estateitem&id=' + this._id);
  }
}
