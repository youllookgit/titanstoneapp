import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var Swiper;

@Component({
  selector: 'device-slider',
  templateUrl: './device.slider.html'
})
export class EsDvsSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
  ) {
  }
  public swiper;
  public Data;
  ngOnInit() {
  }
  ngOnChanges() {
    this.swiper = null; 
    setTimeout(()=>{
        this.swiper =new Swiper('#device', {
          spaceBetween: 5,
          pagination: {
            el: '.pdevice',
          }
        });
    },150);
  }
  ngOnDestroy(){
  }
}
