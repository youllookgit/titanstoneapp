import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var Swiper;

@Component({
  selector: 'intro-slider',
  templateUrl: './intro.slider.html'
})
export class EsItrSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
  ) {
  }
  public swiper;
  public Data;
  ngOnInit() {
  }
  ngOnChanges() {
    this.swiper = null; 
    setTimeout(()=>{
      this.swiper = new Swiper('#intro', {
        spaceBetween: 5,
        pagination: {
          el: '.pintro',
        }
      });
    },150);
  }
  ngOnDestroy(){
    this.swiper = null; 
  }
}
