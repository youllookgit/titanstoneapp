import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var Swiper;

@Component({
  selector: 'floor-slider',
  templateUrl: './floor.slider.html'
})
export class EsFlrSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
  ) {
  }
  public swiper;
  public Data;
  ngOnInit() {
  }
  ngOnChanges() {
    this.swiper = null; 
    setTimeout(()=>{
        this.swiper =new Swiper('#floor', {
          spaceBetween: 15
        });
    },150);
  }
  ngOnDestroy(){
    this.swiper = null; 
  }
}