import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
@Component({
  selector: 'estate-component',
  templateUrl: './estate.component.html'
})
export class EtateComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'',
      txt:'',
      backfun:null
    });
  }
  Data;
  ngOnInit() {
      //地產資料
      this.baseApi.apiPost('Estate/GetList',{}).then(
        (res:any) => {
            // console.log('res',res);
            this.Data = res;
        }
      );
  }

  
}
