import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'insurance-component',
  templateUrl: './insurance.component.html'
})
export class InsuranceComponent extends BaseComponent implements OnInit  {

  constructor(
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B41.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  public CostSum = 0;
  public FData;
  ngOnInit() {
    this.baseApi.apiPost('User/MyInsuranceList',{}).then(
      (res:any)=>{
          console.log('res',res);
          if(res && res.length > 0){
            this.FData = res;
            res.forEach(item => {
              this.CostSum += item['Amount'];
            });
          }else{
            this.FData = [];
          }

      }
    )
  }
}
