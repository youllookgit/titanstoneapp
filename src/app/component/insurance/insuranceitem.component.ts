import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { NavigationEnd,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'insuranceitem-component',
  templateUrl: './insuranceitem.component.html'
})
export class InsuranceitemComponent extends BaseComponent implements OnInit  {

  ID;
  Data;
  constructor(
    public route : ActivatedRoute
  ) {
    super();

    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B41.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  Lang = this.langCode;
  subscribe;
  ngOnInit() {
    //切頁畫面重置 若放constructor api會打兩次
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.GetData(this.route.snapshot.paramMap.get('id'));
    });
    this.GetData(this.route.snapshot.paramMap.get('id'));
  }

  GetData(_id){
    this.ID = _id;
    this.baseApi.apiPost('User/MyInsuranceData', { id: _id}).then(
      (res) => {
        this.Data = res;
        if(this.Data['InsContent'] != '' || this.Data['InsContent'] == null){
          this.Data['InsContent'] = JSON.parse(this.Data['InsContent']);
        }
        console.log('InsContent',this.Data['InsContent']);
      }
    )
  }
  tab = 1;
  TabSet(item){
    this.tab = item;
  }
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }
}
