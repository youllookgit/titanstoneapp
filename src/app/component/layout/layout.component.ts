import { Component, OnInit,NgZone } from '@angular/core';
import{ transformer } from '../../app-routing.animations'
import { BaseComponent } from '../../share/component/base.component';
import { ActivatedRoute } from '@angular/router'
import { Config } from '../../../assets/configuration/config';
import { CordovaService } from '../../share/service/cordova.service';
declare var $;
declare var TweenMax;
declare var device;
declare var cordovaEx;
@Component({
  selector: 'layout-component',
  templateUrl: './layout.component.html',
  animations:[transformer]
})
export class LayoutComponent extends BaseComponent implements OnInit  {
  public mainbg = '';
  public headerCname = '';
  public titleSet = '';
  public right_toggle = false;
  
  public topbarSub;
  public pushSub;
  //share
  public share_toggle = false;
  public shareSub;
  public shareUrl;
  constructor(  
    public activatedRoute : ActivatedRoute,
    public ngzone : NgZone,
    public cordavaSer : CordovaService
  ) {
      super();
      console.log('LayoutComponent constructor');
      //訂閱上全局樣式更新
      this.topbarSub = this.topBar.eventUpdated.subscribe((set)=>{
        console.log('topBar.eventUpdated',set);
        document.getElementById("header").style.cssText = '';
        if(set['bg']){
         this.mainbg = set['bg'];
        }else{
          this.mainbg = '';
        }
        if(set['header']){
         this.headerCname = 'header ' + set['header'];
        }else{
          this.headerCname = 'header';
        }
        if(set['txt']){
         this.titleSet = this.i18n.Trans(set['txt']);
        }else{
          this.titleSet = '';
        }
 
        if(set['backfun']){
          setTimeout(()=>{
            this.IsBackShow = true;
            this.BackEvtTemp = set['backfun'];
          },250);
             
           }else{
            setTimeout(()=>{
              this.IsBackShow = false;
             this.BackEvtTemp = null;
            },250);
         }
         if(set['tabShow'] == false){
           this.tabShow = set['tabShow'];
         }else{
           this.tabShow = true;
         }
         if(set['contact']){
          this.IsCntShow = set['contact'];
        }else{
          this.IsCntShow = false;
        }
         this.SetHeaderStyle();
         this.CountPush();
     });
     //計算目前未讀數
     this.CountPush();
     //訂閱推播更新事件
     this.pushSub = this.cordavaSer.pushUpdated.subscribe(()=>{
      this.CountPush();
     });
     //訂閱分享更新事件
     this.shareSub = this.topBar.shareEvtUpd.subscribe((_url)=>{
       this.shareUrl = _url;
       this.share_toggle = true;
     });

     this.MenuSet();
  }
  public IsBackShow = false;
  public IsCntShow = false;
  public tabShow = true;
  public BackEvtTemp;
  public tabindex = 0;

  ngOnInit() {

    // console.log('Set cordovaEx.PushEvent At CountPush');
    // cordovaEx['PushEvent'] = this.CountPush();

    this.activatedRoute.url.subscribe(url =>{
      //清除設定紀錄
      this.right_toggle = false;
      this.share_toggle = false;
      this.SetHeaderStyle();
      //視窗滾動至頂
      document.getElementById("main").scrollTo(0, 0);
      //選單多語系問題
      this.MenuSet();

      setTimeout(function(){
        //處理ios 換頁異常問題
        var anidom = $('.ng-animating');
        if(anidom.length == 2){
          $('.ng-animating').eq(1).remove();
        }
        if(anidom){
          anidom.removeClass('ng-animating');
        }
      },350);

      setTimeout(function(){
        $('.ng-star-inserted').attr('style','');
      },500);

    });

    this.ngzone.runOutsideAngular(function(){
        var mainObj = $('#main');
        
        //ios
        window.addEventListener('scroll', function(e) {
          //console.log('window scroll');
          var scroll = $(window).scrollTop();
          if(scroll > 120){
            $('.header').addClass('bgdown');
          }else{
            $('.header').removeClass('bgdown');
          }
        });

        // if(Config.CordovaMode){
        //   if(device.platform == "Android"){
        //     //android
        //     window.addEventListener('scroll', function(e) {
        //       //console.log('window scroll');
        //       var scroll = $(window).scrollTop();
        //         if(scroll > -1){
        //           var _opacity = (scroll > 300) ? 1 : (scroll/300);
        //           $('.header').css('background-color','rgba(219, 153, 0,' + _opacity + ')');
        //         }else{
        //           $('.header').css('background-color','rgba(219, 153, 0,1)');
        //         }
        //     });
        //   }else{
        //      //ios
        //      window.addEventListener('scroll', function(e) {
        //       console.log('window scroll');
        //       var scroll = $(window).scrollTop();
        //       if(scroll > 120){
        //         $('.header').addClass('bgdown');
        //       }else{
        //         $('.header').removeClass('bgdown');
        //       }
        //     });
        //   }
        // }else{
        //   //ios
        //   window.addEventListener('scroll', function(e) {
        //     console.log('window scroll');
        //     var scroll = $(window).scrollTop();
        //     if(scroll > 120){
        //       $('.header').addClass('bgdown');
        //     }else{
        //       $('.header').removeClass('bgdown');
        //     }
        //   });
        // }
    });

    // setTimeout(function(){
    //   var s_path = 'M0,25C18.75,21.313,18.75,7.833,37.5,7.833C56.25,7.833,56.313,21.188,75,25H0z';
    //   var e_path = 'M0,25C7.583,24.334,23.75,23,37.5,23C56.25,23,69.584,24.084,75,25H0z';
    //   $('.btm_menu div').click(function(e){
    //     var isact = $(this).attr('class');
    //     if(isact == 'active'){
    //       return;
    //     }

    //     $('.btm_menu div').removeClass('active');
    //     $('.btm_menu div svg path').attr('d',e_path);

    //     $(this).addClass('active');
    //       var p = $(this).find('path')[0];
    //       TweenMax.to(p,0.5, {
    //         attr:{d:s_path},
    //         ease: 'Power1.easeInOut'
    //      });
    //   })
    // },250);
  }
  SetHeaderStyle(){
    document.getElementById("main").className = this.mainbg;
    document.getElementById("header").className = this.headerCname;
    document.getElementById("header").style.cssText = '';
    document.getElementById("title").innerHTML = this.titleSet;
    
  }
  BackEvt(){
    if(typeof this.BackEvtTemp == 'function'){
      this.BackEvtTemp();
    }
  }
  public menu = {
    tab1:'',
    tab2:'',
    tab3:'',
    tab4:'',
    tab5:'',
    cancel:'',
    ctitle:'',
    contact:'',
    online:'',
    sharetype:''
  };
  public MenuSet(){
    //選單多語系問題
    this.menu.tab1 = this.i18n.Trans('Tab.001');
    this.menu.tab2 = this.i18n.Trans('Tab.002');
    this.menu.tab3 = this.i18n.Trans('Tab.003');
    this.menu.tab4 = this.i18n.Trans('Tab.004');
    this.menu.tab5 = this.i18n.Trans('Tab.005');
    //底部聯絡我們
    this.menu.cancel = this.i18n.Trans('B2211.002');
    this.menu.ctitle = this.i18n.Trans('A14.001');
    this.menu.contact = this.i18n.Trans('A14.002');
    this.menu.online = this.i18n.Trans('A14.003');
    this.menu.sharetype = this.i18n.Trans('A12.013');
  }
  public nowIndex = 0;
  prepRouteState(outlet: any) {
    
    var _animate = outlet.activatedRouteData['animation'];
    var tab = outlet.activatedRouteData['tab'];
    var tabno = Number(tab);
    if(tabno > -1 && tabno != this.tabindex){
      this.tabindex = tab;
      this.tabAnimate(tab);
    }
    var _animateParam = {
      value: _animate,
          params: {
            offsetEnter: (_animate == 'isLeft') ? -100 : 100,
            offsetTop:0
      }
     };
    return _animateParam;
  } 
  tabAnimate(index){
    var s_path = 'M0,25C18.75,21.313,18.75,7.833,37.5,7.833C56.25,7.833,56.313,21.188,75,25H0z';
    var e_path = 'M0,25C7.583,24.334,23.75,23,37.5,23C56.25,23,69.584,24.084,75,25H0z';
    var thistab = $('.btm_menu div').eq(index);
    thistab.find('path').attr('d',e_path);
    setTimeout(() => {
      thistab.siblings().removeClass('active');
      thistab.addClass('active');
      var p = $(thistab).find('path')[0];
          TweenMax.to(p,0.5, {
            attr:{d:s_path},
            ease: 'Power1.easeInOut'
         });
    }, 0);
  }

  cToggle(){
    this.right_toggle = !this.right_toggle;
  }
  sToggle(){
    this.share_toggle = !this.share_toggle;
  }
  //推播事件
  public PushCount = 0;
  CountPush = () => {
    this.PushCount = 0;
    var _pushID = this.jslib.GetStorage('pushid');
    this.baseApi.backPost('Notice/UnReadSum',{pushid:_pushID}).then((res:any)=>{
      console.log('Refresh UnReadSum res',res);
      if(res > 0 || res == 0){
        setTimeout(()=>{
          this.PushCount = res;
        },500);
      }
    });
  }
  ngOnDestroy(){
    console.log('LayoutComponent ngOnDestroy');
    this.topbarSub.unsubscribe();
    this.pushSub.unsubscribe();
  }
  
}
