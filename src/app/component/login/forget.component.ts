import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
import { PopupService } from '../../share/service/popup.service';
@Component({
  selector: 'forget-component',
  templateUrl: './forget.component.html'
})
export class ForgetComponent extends BaseComponent implements OnInit  {
  finish = false;
  tempmail = '';
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A112.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  public mainForm: FormGroup;
  ngOnInit() {
     // 建立表單規格內容
     this.mainForm = new FormGroup({
      Mail: new FormControl('', [Validators.required])
    });
  }
  showmsg = '';
  FormSubmit(){
    var postdata = this.mainForm.value;
    this.tempmail = postdata['Mail'];
    this.showmsg = this.i18n.Trans('A113.002');
    console.log('this.showmsg',this.showmsg);
    this.showmsg = this.showmsg.replace('abc12345@gmail.com',this.tempmail);
    this.baseApi.apiPost('User/Forget',postdata).then((res)=>{
      this.finish = true
    },(err)=>{
      this.popup.setConfirm(err['msg']);
    });
  }
  ReSend(){
    this.baseApi.apiPost('User/Forget',{Mail:this.tempmail}).then((res)=>{
      var msg = this.i18n.Trans('E41.012');
      this.popup.setConfirm(msg);
    },(err)=>{
      this.popup.setConfirm(err['msg']);
    });
  }
}
