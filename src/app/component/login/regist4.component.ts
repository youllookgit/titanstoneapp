import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'regist4-component',
  templateUrl: './regist4.component.html'
})
export class Regist4Component extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A19.001',
      backfun:null,
      tabShow:false
    });
  }
  mailmsg = '';
  ngOnInit() {
    var data = this.jslib.GetStorage('regist');
    var mail = data['Act'];
    this.mailmsg = this.i18n.Trans('A19.003');
    this.mailmsg = this.mailmsg.replace('abc12345@gmail.com',mail)
    //clear tempdata
    this.jslib.SetStorage('regist',null);
  };
}
