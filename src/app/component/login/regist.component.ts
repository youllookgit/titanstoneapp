import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'regist-component',
  templateUrl: './regist.component.html'
})
export class RegistComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A16.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  public mainForm: FormGroup;
  ngOnInit() {
    var his = this.jslib.GetStorage('regist');
    var tempmail = '';
    if(his && his['Act']){
      tempmail = his['Act'];
    }
    // 建立表單規格內容
    this.mainForm = new FormGroup({
      Act: new FormControl(tempmail, [Validators.required]),
      Pwd: new FormControl('', [Validators.required,Validators.minLength(6)]),
      PwdCheck: new FormControl('', [Validators.required,Validators.minLength(6)])
    });
  };
  FormSubmit(){
    var _form = this.mainForm.value;
    if(_form['Pwd'] != _form['PwdCheck']){
      this.popup.setConfirm({
        content: this.i18n.Trans('A16.015')
      });
    }else{
      this.jslib.SetStorage('regist',this.mainForm.value);
      this.router.navigate(['layout/regist2']);
    }
  }
}
