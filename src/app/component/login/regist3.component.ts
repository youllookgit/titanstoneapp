import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator,FormArray } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
declare var device;

@Component({
  selector: 'regist3-component',
  templateUrl: './regist3.component.html'
})
export class Regist3Component extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A16.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
  }
  
  public mainForm: FormGroup;
  ngOnInit() {
   
  };
  FormSubmit(){
    //亂寫的別抄
    var prefer =  $('#prefer').serializeObject();
    var _pdata = [];
    for(var i = 1;i <= 7 ;i++){
       var result = prefer[('q' + i)];
       result = (result) ? result : '';
       result = (typeof result == 'object') ? result.join(',')  : result;
       var answer = {
        PType:('q' + i),
        PAnswer:result
       };
       _pdata.push(answer);
    }
    var data = this.jslib.GetStorage('regist');
    var newData = Object.assign(data,{Prefer:_pdata});
    this.jslib.SetStorage('regist',newData);
    //add Device Data
    var ExtendData = {
      DeviceID:'offline',
      Platform:'PC',
      Version:'1.0.0',
      PushID:'no-id',
      Lang:''
    };
    if(this.config.CordovaMode){
      ExtendData.DeviceID = device.uuid;;
      ExtendData.Platform = device.platform;
      ExtendData.Version = device.version;
      ExtendData.PushID = this.jslib.GetStorage('pushid');
      ExtendData.Lang = this.jslib.GetStorage('lang');
    }
    newData = Object.assign(newData,ExtendData);
    //Submit 
    this.baseApi.apiPost('User/Regist',newData).then((res)=>{
      this.router.navigate(['layout/regist4']);
    },(err)=>{
      this.popup.setConfirm(err);
    });
    
  }
}
