import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'regist2-component',
  templateUrl: './regist2.component.html'
})
export class Regist2Component extends BaseComponent implements OnInit  {
  Countries;
  today;
  constructor(  
  ) {
    super();
    this.today = new Date();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A17.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
    //取得國家
    this.baseApi.apiPost('Option/GetOptions',{code:'Country'}).then(
      (res)=>{ this.Countries = res;
        this.CreateForm();
      }
    )
  }

  public mainForm: FormGroup;
  ngOnInit() {
    
  };
  CreateForm(){
    var his = this.jslib.GetStorage('regist');
    // 建立表單規格內容
    this.mainForm = new FormGroup({
      Name: new FormControl(((his['Name'])? his['Name'] : ''), [Validators.required]),
      Birth: new FormControl(((his['Birth'])? his['Birth'] : ''), [Validators.required]),
      Country: new FormControl(((his['Country'])? his['Country'] : ''), [Validators.required]),
      PhonePrefix: new FormControl(((his['PhonePrefix'])? his['PhonePrefix'] : ''), [Validators.required,Validators.maxLength(5),Validators.minLength(2)]),
      Phone: new FormControl(((his['Phone'])? his['Phone'] : ''), [Validators.required,Validators.maxLength(12),Validators.minLength(8)])
    });
  }
  FormSubmit(){
    var _form = this.mainForm.value;
    var data = this.jslib.GetStorage('regist');
    var newData = Object.assign(data,_form);
    //add User img
    if(this.fileUrl != 'assets/img/pdefault.png'){
      newData['ImageUrl'] = this.fileUrl;
    }
    this.jslib.SetStorage('regist',newData);
    this.router.navigate(['layout/regist3']);

  }
  Prefix(){
    var c = this.mainForm.value['Country'];
    if(c != ''){
      var citem = this.Countries.find((item)=>{
        return item['value'] == c;
      });
      if(citem){
        this.mainForm.get('PhonePrefix').setValue('+' + citem['extend1']);
        this.mainForm.updateValueAndValidity();
      }
    }
  }
  
  fileUrl = 'assets/img/pdefault.png';
  UploadHandler() {
    console.log('UploadHandler');
    const Fileinput = document.createElement('input');
    Fileinput.setAttribute('type', 'file');
    Fileinput.setAttribute('accept','image/png, image/gif, image/jpeg, image/bmp');
  
    Fileinput.addEventListener('change', () =>  {
        if (Fileinput.files != null && Fileinput.files[0] != null) {
          //驗大小
          console.log('Imageinput.files',Fileinput.files[0]);
          this.baseApi.apiPostFile('User/ImgUpd',Fileinput.files[0]).then((url:any)=>{
            console.log('Upload res',url);
            this.fileUrl = url;
          },(err)=>{
            this.popup.setConfirm({content:err['msg']});
          });
        }
    });
    Fileinput.click();
  }
  //img to base64
  // ImgConvert(){
  //   var filesSelected = document.getElementById('updimg')['files'];
  //   if (filesSelected.length > 0) {
  //     var fileToLoad = filesSelected[0];
  //     var fileReader = new FileReader();
  //     fileReader.onload = function(fileLoadedEvent:any) {
  //       var srcData = fileLoadedEvent.target.result; // <--- data: base64
  //       var newImage = document.createElement('img');
  //       console.log('srcData',srcData);
  //     }
  //     fileReader.readAsDataURL(fileToLoad);
  //   }
  // }
  DateCheck(){
    var d = this.mainForm.value['Birth'];
    var _d = new Date(d);
    var today = new Date();
    if(_d.getTime() > (today.getTime() + (24*3600*1000))){
      this.popup.setConfirm({
        content:this.i18n.Trans('A17.016'),
        event:()=>{
          this.mainForm.get('Birth').setValue('');
          this.mainForm.updateValueAndValidity();
        }
      });
    }
  }
}
