import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { BaseComponent } from '../../share/component/base.component';
import { PopupService } from '../../share/service/popup.service';
declare var device;
@Component({
  selector: 'login-component',
  templateUrl: './login.component.html'
})
export class LoginComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
  }

  public mainForm: FormGroup;
  ngOnInit() {
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A111.006',
      tabShow:false,
      backfun:()=>{
        this.backpage();
      }
    });
     // 建立表單規格內容
     this.mainForm = new FormGroup({
      Act: new FormControl('', [Validators.required]),
      Pwd: new FormControl('', [Validators.required,Validators.minLength(6)])
    });
  }
  Regist(){
    console.log('regist');
    this.router.navigate(['layout/regist']);
  }
  FormSubmit(){
    //add Device Data
    var ExtendData = {
      DeviceID:'offline',
      Platform:'PC',
      Version:'1.0.0',
      PushID:'no-id',
      Lang:''
    };
    if(this.config.CordovaMode){
      ExtendData.DeviceID = device.uuid;;
      ExtendData.Platform = device.platform;
      ExtendData.Version = device.version;
      ExtendData.PushID = this.jslib.GetStorage('pushid');
      ExtendData.Lang = this.jslib.GetStorage('lang')
    }
    Object.assign(ExtendData,this.mainForm.value);
    this.baseApi.apiPost('User/LogIn',ExtendData).then((res)=>{
      console.log('LogIn res',res);
      this.jslib.SetStorage('user',res);
      this.router.navigate(['layout/home']);
    },(err)=>{
      this.popup.setConfirm(err['msg']);
    });
  }
  
}
