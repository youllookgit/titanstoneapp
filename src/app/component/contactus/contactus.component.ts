import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';

@Component({
  selector: 'contactus-component',
  templateUrl: './contactus.component.html'
})
export class ContactusComponent extends BaseComponent implements OnInit  {
  public Countries;
  public Estates;
  public CntType;
  public Times;
  EstatesShow = false;
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A15.001',
      backfun:()=>{
        //this.topBar.set({backfun:null});
        this.backpage();
      },
      tabShow:false
    });
    

    //get Country
    this.baseApi.apiPost('Option/GetOptions',{code:'Country'}).then(
      (res)=>{ this.Countries = res;}
    );
    //联络主题
    this.baseApi.apiPost('Option/GetOptions',{code:'Contact'}).then(
      (res)=>{ this.CntType = res;}
    );
    //時段
    this.baseApi.apiPost('Option/GetOptions',{code:'Time'}).then(
      (res)=>{ this.Times = res;}
    );
    //地產項目
    this.baseApi.apiPost('Estate/GetList',{}).then(
      (res:any) => {
          this.Estates = res;
          this.CreateForm();
      }
    );
    
  }
  ngOnInit() {

  }
  public mainForm: FormGroup;
  CreateForm(){
    //預設User資料
    if(this.IsLogin()){
      var user = this.GetUser();
      this.mainForm = new FormGroup({
        Name: new FormControl(user['Name'], [Validators.required]),
        Email: new FormControl(user['Act'], [Validators.required]),
        Country: new FormControl(user['Country'], [Validators.required]),
        Prefix: new FormControl(user['PhonePrefix'], [Validators.required,Validators.maxLength(5),Validators.minLength(2)]),
        Phone: new FormControl(user['Phone'], [Validators.required,Validators.maxLength(12),Validators.minLength(8)]),
        SocialID: new FormControl(''),
        ContactType:new FormControl('',[Validators.required]),
        ContactTime:new FormControl('',[Validators.required]),
        EstateId: new FormControl(''),
        Note: new FormControl('')
      });
    }else{
      this.mainForm = new FormGroup({
        Name: new FormControl('', [Validators.required]),
        Email: new FormControl('', [Validators.required]),
        Country: new FormControl('', [Validators.required]),
        Prefix: new FormControl('', [Validators.required,Validators.minLength(2)]),
        Phone: new FormControl('', [Validators.required,Validators.minLength(8)]),
        SocialID: new FormControl(''),
        ContactType:new FormControl('',[Validators.required]),
        ContactTime:new FormControl('',[Validators.required]),
        EstateId: new FormControl(''),
        Note: new FormControl('')
      });
    }
    //從地產來
    var estateid = this.jslib.UrlParam()['estateid'];
    if(estateid){
      this.mainForm.get('EstateId').setValue(estateid);
      this.mainForm.get('ContactType').setValue('1');
      this.EstatesShow = (this.mainForm.value['ContactType'] == '1');
      this.mainForm.updateValueAndValidity();
    }
    //指定選項
    var itemid = this.jslib.UrlParam()['item'];
    if(itemid){
      this.mainForm.get('ContactType').setValue(itemid);
      this.mainForm.updateValueAndValidity();
    }

    
  }
  FormSubmit(){
    var form = this.mainForm.value;
    this.baseApi.apiPost('Contact/Post', form).then(
      (res) => {
        this.popup.setConfirm({
          title:this.i18n.Trans('D332.001'),
          content:this.i18n.Trans('D332.005'),
          event:()=>{
            this.backpage();
          }
        });
      }
    )
    console.log('form',form);
  }
  Prefix(){
    var c = this.mainForm.value['Country'];
    if(c != ''){
      var citem = this.Countries.find((item)=>{
        return item['value'] == c;
      });
      if(citem){
        this.mainForm.get('Prefix').setValue('+' + citem['extend1']);
        this.mainForm.updateValueAndValidity();
      }
    }
  }
  TypeChange(){
    this.EstatesShow = (this.mainForm.value['ContactType'] == '1');
  }
}
