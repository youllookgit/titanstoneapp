import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var $;
@Component({
  selector: 'trend-component',
  templateUrl: './trend.component.html'
})
export class TrendComponent extends BaseComponent implements OnInit {
  trendtype = '';
  typename = this.i18n.Trans('D21.002');
  trendoption;
  trendlist;
  trendfirst;
  menuHidden = false;
  constructor() {
    super();
    this.topBar.set({
      bg: '',
      header: 'mainbg',
      txt: 'D21.001',
      backfun: () => {
        this.backpage();
        //this.router.navigate(['/layout/find']);
      },
      tabShow: false
    });
    this.GetData();
    this.menuHidden = !this.IsLogin();
  }
  ngOnInit() {
    
  }
  GetData(){
      //取得理財趨勢
      this.baseApi.apiPost('Trend/GetTrendList', { trendtype: this.trendtype}).then(
        (res) => {
          this.trendoption = res['option'];
          
          var list = res['list'];
          if(list && list.length >= 1){
            this.trendfirst = list[0];
            this.trendlist = list.slice(1);//移除第一個
          }else{
            this.trendfirst = null;
            this.trendlist = [];
          }
        }
      );
  };
  optionchange() {
    var c = this.trendtype;
    this.GetData();
    if (c != '') {
      var citem = this.trendoption.find((item) => {
        return item['value'] == c;
      });
      this.typename = citem.name;
    }
    else {
      this.typename = this.i18n.Trans('D21.002');
    }
  }
}


