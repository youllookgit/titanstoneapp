import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { NavigationEnd,ActivatedRoute } from '@angular/router';
declare var Swiper;

@Component({
  selector: 'trendDetail-component',
  templateUrl: './trendDetail.component.html'
})
export class TrendDetailComponent extends BaseComponent implements OnInit {
  id;
  trendData;
  swiper;
  subscribe;
  constructor(
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg: '',
      header: '',
      txt: 'D21.001',
      backfun: () => {
        this.backpage();
        //this.router.navigate(['/layout/trend']);
      },
      tabShow: false
    });
  }
  ngOnInit() {
    //切頁畫面重置 若放constructor api會打兩次
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.GetTrendData(this.route.snapshot.paramMap.get('id'));
    });
    this.GetTrendData(this.route.snapshot.paramMap.get('id'));
  }
  GetTrendData(id){
    this.id = id;
    this.baseApi.apiPost('Trend/GetTrendData', { id: this.id}).then(
      (res) => {
        this.trendData = res;
        this.swiper = null;
        window.scrollTo(0, 0);
        setTimeout(()=>{
          this.swiper = new Swiper('#trend', {
            spaceBetween: 15
          });
        },250);
      }
    )
  }
  After(id){
    if(this.trendData.AfterValid){
      this.router.navigate(['/layout/trend/detail/' + id]);
    }else{
      this.popup.setConfirm('請登入才成查看更多');
    }
  }
  ShareEvt(){
    this.topBar.OpenShare('type=trend&id=' + this.id);
  }
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }
}
