import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'faqitem-component',
  templateUrl: './faqitem.component.html'
})
export class FaqItemsComponent extends BaseComponent  implements OnInit  {

  constructor(  
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E71.001',
      backfun:()=>{
        this.router.navigate(['/layout/faq']);
      },
      tabShow:false,
      contact:true
    });
    var id = this.route.snapshot.paramMap.get('id');
    this.GetData(id);
  }

  ngOnInit() {
  
  }
  public list;
  public GetData(_id) {
    this.baseApi.apiPost('Faq/GetDetail', { id: _id}).then(
      (res) => {
        this.list = res;
      }
    )
  }
}

