import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
@Component({
  selector: 'faq-component',
  templateUrl: './faq.component.html'
})
export class FaqComponent extends BaseComponent implements OnInit  {
  public list;
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'E71.001',
      backfun:()=>{
        this.router.navigate(['/layout/set']);
      },
      tabShow:false,
      contact:true
    });
    this.baseApi.apiPost('Faq/GetList',{}).then(
      (res:any) => {
          this.list = res;
      }
    )
  }

  ngOnInit() {
  }


}
