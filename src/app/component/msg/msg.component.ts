import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { CordovaService } from '../../share/service/cordova.service';
@Component({
  selector: 'msg-component',
  templateUrl: './msg.component.html'
})
export class MsgComponent extends BaseComponent implements OnInit  {

  public ListData = [];
  public Options;
  public PushID = '';
  constructor(  
    public cordavaSer : CordovaService
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'A13.001',
      backfun:()=>{
        // this.topBar.set({backfun:null});
        this.backpage();
      },
      tabShow:false
    });
    this.GetData();
  }
  ngOnInit() {

  }
  GetData(){
    this.PushID = this.jslib.GetStorage('pushid');
    this.baseApi.apiPost('Notice/GetNoticeList',{pushid:this.PushID}).then((res:any)=>{
      console.log('GetNoticeList res',res);
      if(res && res.length > 0){
        this.ListData = res;
      }
    },(err)=>{
      this.popup.setConfirm(err['msg']);
    });
  }
  Read(item,index){
    console.log('item,index',item,index);
    var btntxt = (item.read) ? 'A13.012' : 'A13.014';
    this.popup.setConfirm({
      title:item['title'],
      content:item['content'],
      checkTxt:btntxt,
      event:()=>{
          console.log('_index',index,this.ListData[index]);
          var d = this.ListData[index];
          if(!d['read']){
            this.ListData[index].read = true;
            this.baseApi.backPost('Notice/Read',{pushItemid:[this.ListData[index]['id']],pushid:this.PushID}).then((res:any)=>{
              console.log('GetNoticeList res',res);
            });
           // this.cordavaSer.setPushUpdate(true);
          }
      }
    })
  }
  AllRead(){
    var ids = [];
    this.ListData.forEach(p =>{
      if(p.read == false){
        ids.push(p['id']);
        p['read'] = true;
      }
    });
    this.baseApi.backPost('Notice/Read',{pushItemid:ids,pushid:this.PushID}).then((res:any)=>{
      console.log('Read res',res);
    });
    //this.cordavaSer.setPushUpdate(true);
  };
  AllDel(){
    this.popup.setConfirm({
     content:this.i18n.Trans('A13.011'),
     cancelTxt:'A13.013',
     event:()=>{
      var ids = [];
      this.ListData.forEach(p =>{
        ids.push(p['id']);
      });
      this.baseApi.backPost('Notice/Delete',{pushItemid:ids,pushid:this.PushID}).then((res:any)=>{
        console.log('Read res',res);
      });
      this.ListData = [];
      //this.cordavaSer.setPushUpdate(true);
     } 
    });
  }
  
}
