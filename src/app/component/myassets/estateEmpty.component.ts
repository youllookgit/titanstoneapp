import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'estateEmpty-component',
  templateUrl: './estateEmpty.component.html'
})
export class EstateEmptyComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'我的資产',
      backfun:()=>{
        this.router.navigate(['/layout/myassets']);
      },
      tabShow:false
    });
  }
ngOnInit() {

}
  
}
