import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'estateFinish-component',
  templateUrl: './estateFinish.component.html'
})
export class EstateFinishComponent extends BaseComponent implements OnInit  {

  public Temp;
  public SaleType;
  public monthName = ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'];
  constructor(  
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'',
      txt:'',
      backfun:()=>{
        this.router.navigate(['/layout/myestate']);
      },
      tabShow:false
    });
    this.Temp =  this.jslib.GetStorage('tempest');
    //Temp.SaleType 2.成屋(畫面不顯示) 3.代租中 4.包租中 5.已出租
    this.SaleType = this.Temp['SaleType'];
    console.log('Temp',this.Temp);
  }
  ngOnInit() {
    this.GetData(this.route.snapshot.paramMap.get('id'));
  }
  public tab = 1;
  tabset(index){
    if(index != this.tab){
      this.tab = index;
    }
  }

  public Data;
  public SaleData;
  public PriceSum = 0;
  public Income = [];
  public YearData = [];
  public MngData;
  public RentData;
  GetData(_id){
    this.baseApi.apiPost('User/MyEstateItem', { id: _id}).then(
      (res) => {
        console.log('MyEstateItem',res);
        this.Data = res;
        this.SaleData = res['Sale'];
        this.MngData = res['Manage'];
        this.RentData = res['Rent'];
        //總價
        this.PriceSum = Number(this.SaleData['PriceSum']);
        //設定收入
        if(res['Income'] && res['Income'].length > 0){
          this.Income = res['Income'];
          this.Income.forEach(actitem => {
            actitem['active'] = false;
          });
          this.Income[0]['active'] = true;
          this.YearData = this.Income[0]['Data'];
          this.CountIncome(this.Income[0]);
        }else{
          this.Income = [];
        }
      }
    )
  }
  ViewImcome(index){
    console.log('ViewImcome',index)
    this.Income.forEach(citem => {
      citem['active'] = false;
    });
    this.Income[index]['active'] = true;
    this.YearData = this.Income[index]['Data'];
    this.CountIncome(this.Income[index]);
  }

  Yavg = 0; //年報酬率
  Ysum = 0; //結算金額
  //計算收益
  CountIncome(data){
    this.Yavg = 0;
    this.Ysum = 0;
    var _sumIn = 0;
    var _sumOut = 0;
    if(data['Data']){
      data['Data'].forEach( c_item => {
         var _in = Number(c_item['Income']);
         if(_in > 0){
          _sumIn = _sumIn + _in;
         }
         var _out = Number(c_item['Expense']);
         if(_out > 0){
          _sumOut = _sumOut + _out;
        }
      });
      //結算金額
      this.Ysum = _sumIn - _sumOut;
      //年報酬率
      if(this.PriceSum > 0){
        this.Yavg = (this.Ysum / this.PriceSum)*100;
      }
    }
  }
}
