import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'estate-component',
  templateUrl: './estate.component.html'
})
export class MyEstateComponent extends BaseComponent implements OnInit  {

  public monCn = ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'];
  public monEn = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Ang','Sep','Oct','Nov','Dec'];
  public monLang = [];
  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B21.001',
      backfun:()=>{
        this.router.navigate(['/layout/myassets']);
      },
      tabShow:false
    });

    if(this.i18n.getLangCode() == 'en_US'){
      this.monLang = this.monEn;
    }else{
      this.monLang = this.monCn;
    }
  }
  

  public LastMonth;
  public Sum;
  public Income;
  public Estate;
  public EstateList;
  public chartData;
  ngOnInit() {
    this.baseApi.apiPost('User/MyEstate',{}).then(
      (res)=>{
      console.log('MyEstate',res);
      this.Estate = res;
      //count
      this.Income = res['Income'];
      var _label = [];
      var _data = this.Income;
      var m = 0;
      this.Income.forEach(mdata => {
        _label.push(this.monLang[m]);
        m = m + 1;
      });

      if(_label.length == 0){
        var thisM = res['nowMonth'];
        for(var _m = 0;_m < thisM ; _m ++){
          _label.push(this.monLang[_m]);
          _data.push(0);
        }
        this.chartData = {
          label:_label,
          data:_data
         };
      }else{
        this.chartData = {
          label:_label,
          data:_data
        };
      }
      
      this.LastMonth = this.monLang[this.Income.length-1];
      this.Sum = this.Income.reduce((a,b)=>a+b);

      this.EstateList = res['MyEstate'];
    });

    // this.chartData = {
    //   label:['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
    //   data:[1000,2000,3000,2000,1450,5000,2000,3000,4000,1000,4250,3000]
    // };
  }
  View(estate){
    this.jslib.SetStorage('tempest',estate);
    if(estate['SaleType'] == '1'){
      //預售
      this.router.navigate(['/layout/estateUnFinish/' + estate.ID]);
    }else{
      this.router.navigate(['/layout/estateFinish/' + estate.ID]);
    }

  }
}
