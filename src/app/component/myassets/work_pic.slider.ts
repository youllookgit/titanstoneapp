import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
declare var $;
declare var Swiper;

@Component({
  selector: 'work-slider',
  templateUrl: './work_pic.slider.html'
})
export class WorkSliderComponent implements OnInit  {
  @Input() SourceData;
  constructor(  
  ) {
  }
  public swiper;
  public title = '';
  public msg = '';
  ngOnInit() {
  }
  
  ngOnChanges() {
    //this.swiper = null;
    //this.swiper.update() 
    if(this.SourceData && this.SourceData.length > 0){
      setTimeout(()=>{
        if(typeof this.swiper == 'undefined'){
          this.swiper = new Swiper('#work', {
            spaceBetween: 5,
            pagination: {
              el: '.work',
            }
          });
          this.swiper.on('slideChange',  () => {
            this.title = this.SourceData[this.swiper.realIndex]['title'];
            this.msg = this.SourceData[this.swiper.realIndex]['desc'];
          });

        }else{
          this.swiper.update();
          this.swiper.slideTo(0, 0.5, false);
        }
        
        this.title = this.SourceData[0]['title'];
        this.msg = this.SourceData[0]['desc'];
  
        
      },150);
    }
  }
  ngOnDestroy(){
    this.swiper = null; 
  }
}