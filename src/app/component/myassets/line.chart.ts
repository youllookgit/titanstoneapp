import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
declare var $;
declare var Chart;

@Component({
  selector: 'line-chart',
  templateUrl: './line.chart.html'
})
export class LineChartComponent implements OnInit  {
  @Input() SourceData;
  
  constructor(  
  ) {
  }
  public chart;
  public Data;
  ngOnInit() {
  }
  ngOnChanges() {
  
    this.chart = null; 
    var _label = this.SourceData['label']; //['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月']
    var _data = this.SourceData['data']; //[1000,2000,3000,2000,1450,5000,2000,3000,4000,1000,4250,3000]
    setTimeout(()=>{
      var canvas:any;
      canvas = document.getElementById("Lchart");
      var ctx = canvas.getContext("2d");
      var Setdata = {
              labels: _label,
              datasets: [{
                  backgroundColor: '#FACE34',
                  lineTension: 0,
                  borderColor:'#FACE34',
                  borderWidth:1.5,
                  fill: false,
                  pointBorderWidth:3.5,
                  data: _data
              }]
          };
      this.chart = new Chart(ctx, {
          type: 'line',
          data: Setdata,
          options:{

            legend: {
              display: false,
            },
            scales: {
              xAxes: [{
                      gridLines: {
                        display: false,
                      },
                      ticks: {
                        fontSize:10,
                        fontColor: '#cccccc',
                        fontFamily:'PingFang SC'
                      }
                  }],
              yAxes: [{
                      gridLines: {
                        drawBorder: false,
                      },
                      ticks: {
                          fontSize:11,
                          fontColor: '#cccccc',
                          beginAtZero: true,
                          padding: 8
                          //,max: 6000
                      }
                  }]
             },
          }
        });
    },150);
  }

  ngOnDestroy(){
    this.chart = null; 
  }
}