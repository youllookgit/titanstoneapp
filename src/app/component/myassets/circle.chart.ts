import { Component, OnInit ,NgZone,Input,OnChanges } from '@angular/core';
declare var $;
declare var Chart;

@Component({
  selector: 'circle-chart',
  templateUrl: './circle.chart.html'
})
export class CircleChartComponent implements OnInit  {
  @Input() SourceData;
  
  constructor(  
  ) {
  }
  public chart;
  public Data;
  public percent = 0;
  ngOnInit() {
  }
  ngOnChanges() {
    this.chart = null; 
    if(this.SourceData){
      var _label = this.SourceData['label']; //["基金", "地產", "保險"]
      var _data = this.SourceData['data']; // [25, 55,15]

      var _p = _data.reduce((a,b)=>a+b);
      this.percent = (_p > 0) ? 100 : 0;
      if(this.percent == 0){
        setTimeout(()=>{
          var canvas:any;
          canvas = document.getElementById("Cchart");
          var ctx = canvas.getContext("2d");
    
          var Setdata = {
                  datasets: [{
                    backgroundColor: ['#DBE3E6'],
                    borderColor: '#fff',
                    data: [1]
                }]
          };
          this.chart = new Chart(ctx, {
              type: 'doughnut',
              data: Setdata,
              options: {
                cutoutPercentage: 80,
                legend: {
                  display: false,
                },
                tooltips: {enabled: false},
                layout: {
                  padding: {
                      left: '50',
                      right:'50',
                      top: '50',
                      bottom: '50'
                  }
              }
              }
          });
    
        },150);
      }else{
        setTimeout(()=>{
          var canvas:any;
          canvas = document.getElementById("Cchart");
          var ctx = canvas.getContext("2d");
    
          var Setdata = {
                  datasets: [{
                    backgroundColor: ['#FACE34', '#FFE498', '#D9A41F'],
                    borderColor: '#fff',
                    data: _data
                }]
          };
    
          this.chart = new Chart(ctx, {
              type: 'doughnut',
              data: Setdata,
              options: {
                cutoutPercentage: 80,
                legend: {
                  display: false,
                },
                tooltips: {enabled: false},
                layout: {
                  padding: {
                      left: '50',
                      right:'50',
                      top: '50',
                      bottom: '50'
                  }
              }
              }
          });
    
        },150);
      }

    }
  }

  ngOnDestroy(){
    this.chart = null; 
  }
}