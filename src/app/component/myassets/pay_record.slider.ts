import { Component, OnInit ,NgZone,Input,Output,EventEmitter,OnChanges } from '@angular/core';
declare var $;
declare var Swiper;

@Component({
  selector: 'pay-record',
  templateUrl: './pay_record.slider.html'
})
export class PayRecordComponent implements OnInit  {
  @Input() SourceData;
  @Output() ChangeData = new EventEmitter();
  constructor(  
  ) {
  }
  public swiper;
  ngOnInit() {
  }
  
  ngOnChanges() {
    this.swiper = null; 
    
    setTimeout(()=>{
      this.swiper = new Swiper('#pay', {
        // spaceBetween: 10,
        effect: 'coverflow',
          grabCursor: true,
          centeredSlides: true,
          slidesPerView: 'auto',
          coverflowEffect: {
              rotate: 0,
              stretch: 0,
              depth: 60,
              modifier: 1,
              slideShadows : true,
        }
      });
      var nowindex = this.SourceData.findIndex(item=>{return item.nowIndex});
      console.log('nowindex',nowindex,this.swiper);
      if(nowindex >= 0){
        //切到第N個
        this.swiper.slideTo(nowindex, 0.5, false);
      }

      this.swiper.on('slideChange',  () => {
        var index = this.swiper.realIndex;
        this.ChangeData.emit(this.SourceData[index]);
      });
    },150);
  }
  ngOnDestroy(){
    this.swiper = null; 
  }
}