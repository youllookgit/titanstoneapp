import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { ActivatedRoute } from '@angular/router';
declare var $;
declare var Swiper;
@Component({
  selector: 'estateUnFinish-component',
  templateUrl: './estateUnFinish.component.html'
})
export class EstateUnFinishComponent extends BaseComponent implements OnInit  {

  public Temp;
  constructor(  
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'',
      txt:'',
      backfun:()=>{
        this.router.navigate(['/layout/myestate']);
      },
      tabShow:false
    });
    this.Temp =  this.jslib.GetStorage('tempest');
}
  
public Data;
public SaleData;
public Pay; //繳款資料
public workData; //施工紀錄圖片
public payRecord;//繳款資訊
ngOnInit() {
  this.GetData(this.route.snapshot.paramMap.get('id'));

    // this.workData = [
    //   {
    //     img:'assets/img/temp/sport1.jpg',
    //     title:'第一层土方开挖及清运',
    //     desc:'深入房下11米，直入岩盘的基桩，增加建筑基础的承载能力。国际级安全规范之钢筋、混凝土。'
    //   },
    //   {
    //     img:'assets/img/temp/sport2.jpg',
    //     title:'第二层土方清运',
    //     desc:'直入岩盘的基桩，增加建筑基础的承载能力。国际级安全规范之钢筋、混凝土。'
    //   },
    //   {
    //     img:'assets/img/temp/sport3.jpg',
    //     title:'第三层土方开挖及清运',
    //     desc:'深入房下11米，增加建筑基础的承载能力。国际级安全规范之钢筋、混凝土。'
    //   },
    // ];
    // this.payRecord = [
    //   {
    //     period:'第二期',
    //     title:'完工日',
    //     date:'2019/06/28',
    //     nowIndex:false,
    //     finish:true,
    //     desc:'第二层土方开挖及清运，单项工程有预留柱打凿完成90%，承台浇筑完成量30%'
    //   },
    //   {
    //     period:'第三期',
    //     title:'進行中',
    //     date:'2019/06/28',
    //     nowIndex:true,
    //     finish:false,
    //     desc:'第单项工程有预留柱打凿完成90%，承台浇筑完成量40%'
    //   },
    //   {
    //     period:'第四期',
    //     title:'完工日',
    //     date:'2019/06/28',
    //     nowIndex:false,
    //     finish:false,
    //     desc:'第三层土方开挖及清运，单项工程有预留柱打凿完成90%，承台浇筑完成量60%'
    //   },
    // ];
    // this.structMsg = this.payRecord[0].desc;
}

  GetData(_id){
    this.baseApi.apiPost('User/MyEstateItem', { id: _id}).then(
      (res) => {
        console.log('MyEstateItem',res);
        this.Data = res;
        this.SaleData = res['Sale'];
        this.Pay = res['Pay'];
        this.SetSchedule(res['Schedule'])
      }
    )
  }
  SetSchedule(sdata){
    if(sdata && sdata.length > 0){
      var paySch = [];
      sdata.forEach(_sd => {
        //1.尚未动工 2.施工中 3.已完工
        var _stype = _sd['ScheduleType'];
        var sobj = {
          period: _sd['SchNo'],
          title: _sd['ScheduleTypeName'],
          date: _sd['EndDate'],
          nowIndex: (_stype == '2'),
          finish:false,
          name: _sd['Name'],
          desc: _sd['Brief'],
          work:_sd['work']
        };
        if(_stype == '1'){
          sobj.finish = false;
        }
        if(_stype == '2'){
          sobj.finish = false;
        }
        if(_stype == '3'){
          sobj.finish = true;
        }
        paySch.push(sobj);
      });

      this.payRecord = paySch;
      this.structTitle = this.payRecord[0].name;
      this.structMsg = this.payRecord[0].desc;
      this.SetWork(this.payRecord[0].work);
    }
  }
  SetWork(wdata){
    if(wdata && wdata.length > 0){
      var wSch = [];
      wdata.forEach(_wd => {
        var wobj = {
          //     img:'assets/img/temp/sport3.jpg',
          //     title:'第三层土方开挖及清运',
          //     desc:'深入房下11米，增加建筑基础的承载能力。国际级安全规范之钢筋、混凝土。'
          img: _wd['Image'],
          title: _wd['Name'],
          desc: _wd['Brief']
        };
        wSch.push(wobj);
      });
      this.workData = wSch;
    }else{
      wdata = [];
      this.workData = wdata;
    }
    
  }
  public tab = 1;
  tabset(index){
    if(index != this.tab){
      this.tab = index;
    }
  }

  public workpicOpen = false;
  ToggleWork(){
    this.workpicOpen = !this.workpicOpen;
  }
  public structTitle = '';
  public structMsg = '';

  periodChange(data){
     this.structTitle = data.name;
     this.structMsg = data.desc;
     var _w = data['work'];
     this.SetWork(_w);
  }
}
