import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'myassets-component',
  templateUrl: './myassets.component.html'
})
export class MyassetsComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'B11.001',
      backfun:null
    });
  }
public chartData;
ngOnInit() {
  var islogin = this.IsLogin();
  if(!islogin){
    this.router.navigate(['/layout/login']);
  }
  //驗證是否有身分證功能
  if(islogin){
    var _user = this.GetUser();
    if(_user['IdentityNo'] == ''){
      this.popup.setConfirm({
        content: this.i18n.Trans('B11.006'),
        checkTxt:this.i18n.Trans('B11.009'),
        cancelTxt:this.i18n.Trans('B11.008'),
        event:()=>{
          this.router.navigate(['/layout/contactus'], {queryParams: {item:'5'}});
        },
        cancelEvent:()=>{
          this.backpage();
        }
      });
    }
  }
  if(islogin){
    this.baseApi.apiPost('User/Chart',{}).then(
      (res:any)=>{
        console.log('Chart',res);
        var _label = [];
        var _number = [];
        res.forEach(item => {
          _label.push(item['Name']);
          _number.push(item['Sum']);
        });
        this.chartData = {
          label:_label,
          data:_number
        };
      }
    )
  }
  // this.chartData = {
  //   label:["基金", "地產", "保險"],
  //   data:[25, 55,15]
  // };
}
  
}
