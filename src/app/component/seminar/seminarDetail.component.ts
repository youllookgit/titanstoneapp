import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { NavigationEnd,ActivatedRoute } from '@angular/router';

declare var Swiper;
@Component({
  selector: 'seminarDetail-component',
  templateUrl: './seminarDetail.component.html'
})

export class SeminarDetailComponent extends BaseComponent implements OnInit  {
  public _User;
  FirstData;
  SeminarData;
  EstateObj;
  Process;
  Traffic;
  subscribe;
  constructor(
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'',
      txt:'D31.001',
      backfun:()=>{
        this.backpage();
        //this.router.navigate(['/layout/seminar']);
      },
      tabShow:false
    });
  }
  ngOnInit() {
    //切頁畫面重置 若放constructor api會打兩次
    this.subscribe = this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      this.getSeminarData(this.route.snapshot.paramMap.get('id'));
    });
    var param = this.route.snapshot.paramMap.get('id');
    this.getSeminarData(param);
  }
  getSeminarData(_id){
    this.baseApi.apiPost('Seminar/GetSeminar', { id:_id}).then(
      (res) => {
        this.FirstData = res;
        this.SeminarData = res['related'];
        this.EstateObj = res['EstateObj'];
        if(res['Process']){
          this.Process = JSON.parse(res['Process']);  //活動流程
        }
        if(res['Traffic']){
          this.Traffic = JSON.parse(res['Traffic']);  //交通
        }
        //資料取回來再執行
        setTimeout(function(){
          var swiperdevice = new Swiper('#device', {
            autoHeight: true,
            spaceBetween: 5,
            pagination: {
              el: '.device',
            }
          });
          var swiperFloor = new Swiper('#floor', {
            autoHeight: true,
              spaceBetween: 15
            });
        },250);

      }
    )
  }
  ShareEvt(){
    this.topBar.OpenShare('type=seminar&id=' + this.FirstData.ID);
  }
  ngOnDestroy(){
    this.subscribe.unsubscribe();
  }
}
