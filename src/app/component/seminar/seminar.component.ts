import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'seminar-component',
  templateUrl: './seminar.component.html'
})
export class SeminarComponent extends BaseComponent implements OnInit {

  ActOptionData;//類型選項
  ActSessionData;//場次選項
  FirstActData;//第一筆資料
  ActData;//活動資料

  optiontype = '';
  typename = this.i18n.Trans('D31.006');
  sessiontype = '';
  sessionname = this.i18n.Trans('D31.007');
  
  constructor(
  ) {
    super();
    this.topBar.set({
      bg: '',
      header: 'mainbg',
      txt: 'D31.001',
      backfun: () => {
        this.backpage();
        //this.router.navigate(['/layout/find']);
      },
      tabShow: false
    });
  }
  ngOnInit() {
    this.getSeminarData();
  }

  getSeminarData() {
    
    this.baseApi.apiPost('Seminar/GetSeminarData', { ActType: this.optiontype, ActArea: this.sessiontype}).then(
      (res) => {
        this.ActOptionData = res['typeOpt'];
        this.ActSessionData = res['areaOpt'];
        var list = res['list'];
        if(list && list.length >= 1){
          this.FirstActData = list[0];
          this.ActData = list.slice(1);//移除第一個
        }else{
          this.FirstActData = null;
          this.ActData = [];
        }
      }
    )
  }

  optionchange() {
    var c = this.optiontype;
    if (c != '') {
      var citem = this.ActOptionData.find((item) => {
        return item['value'] == c;
      });
      this.typename = citem.name;
    }
    else {
      this.typename = this.i18n.Trans('D31.006');
    }
    this.sessiontype = '';
    this.sessionname = this.i18n.Trans('D31.007');
    //this.getOptionData();
    this.getSeminarData();
  }
  sessionchange() {

    var c = this.sessiontype;
    //this.getOptionData();
    if (c != '') {
      var citem = this.ActSessionData.find((item) => {
        return item['value'] == c;
      });
      this.sessionname = citem.name;
    }
    else {
      this.sessionname = this.i18n.Trans('D31.007');
    }

    this.getSeminarData();
  }
}
