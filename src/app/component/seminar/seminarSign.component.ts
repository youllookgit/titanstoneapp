import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
import { FormGroup, FormControl, Validators, MaxLengthValidator } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'seminarSign-component',
  templateUrl: './seminarSign.component.html'
})
export class SeminarSignComponent extends BaseComponent implements OnInit  {
  public Slist;
  public Countries;
  public Sid;
  constructor(  
    public route : ActivatedRoute
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'D331.001',
      backfun:()=>{
        this.backpage();
      },
      tabShow:false
    });
    this.Sid = this.route.snapshot.paramMap.get('id');
    //取得國家
    this.baseApi.apiPost('Option/GetOptions',{code:'Country'}).then(
      (res)=>{ this.Countries = res;}
    )
    //取得說明會列表
    this.baseApi.apiPost('Seminar/SignList', { id:this.Sid}).then(
      (res) => {
       this.Slist = res;
       this.ShowSelect();
       this.CreateForm();
      }
    )
  }
  ngOnInit() {
    
  }

  // 建立表單
  public mainForm: FormGroup;
  CreateForm(){
    //預設User資料
    if(this.IsLogin()){
      var user = this.GetUser();
      this.mainForm = new FormGroup({
        id: new FormControl(this.Sid),
        Name: new FormControl(user['Name'], [Validators.required]),
        Email: new FormControl(user['Act'], [Validators.required]),
        Country: new FormControl(user['Country'], [Validators.required]),
        Prefix: new FormControl(user['PhonePrefix'], [Validators.required,Validators.maxLength(5),Validators.minLength(2)]),
        Phone: new FormControl(user['Phone'], [Validators.required,Validators.maxLength(12),Validators.minLength(8)]),
        Note: new FormControl('')
      });
    }else{
      this.mainForm = new FormGroup({
        id: new FormControl(this.Sid),
        Name: new FormControl('', [Validators.required]),
        Email: new FormControl('', [Validators.required]),
        Country: new FormControl('', [Validators.required]),
        Prefix: new FormControl('', [Validators.required,Validators.maxLength(5),Validators.minLength(2)]),
        Phone: new FormControl('', [Validators.required,Validators.maxLength(12),Validators.minLength(8)]),
        Note: new FormControl('')
      });
    }
    

  }
  FormSubmit(){
    var form = this.mainForm.value;
    var extend = {
      Count:this.count,
      ActName:this.selected['Name']+'-'+this.selected['AreaName'],
      ActDate:this.selected['ActDate']+' '+this.selected['ActTime'],
      ActAddr:this.selected['ActPosition']+' '+this.selected['ActAddr'],
    }
    var post = Object.assign(form,extend);
    this.baseApi.apiPost('Seminar/SignPost', post).then(
      (res) => {
        this.popup.setConfirm({
          title:this.i18n.Trans('D332.001'),
          content:this.i18n.Trans('D332.002'),
          checkTxt:this.i18n.Trans('D331.015'),
          cancelTxt:this.i18n.Trans('D332.003'),
          event:()=>{
            this.backpage();
          },
          cancelEvent:()=>{
            this.AddCanlendar();
            this.backpage();
          }
        });
      }
    )
    console.log('form',form);
  }
  selected;
  ShowSelect(){
    var data = this.Slist.find(item=>{return item.id == this.Sid});
    this.selected = data;
  }
  Prefix(){
    var c = this.mainForm.value['Country'];
    if(c != ''){
      var citem = this.Countries.find((item)=>{
        return item['value'] == c;
      });
      if(citem){
        this.mainForm.get('Prefix').setValue('+' + citem['extend1']);
        this.mainForm.updateValueAndValidity();
      }
    }
  }
  count = 1;
  Add(){
    this.count = this.count + 1;
  }
  Less(){
    if(this.count >= 2){
      this.count = this.count - 1;
    }
  }
  AddCanlendar(){
    if(this.config.CordovaMode){
      var startDate = new Date(this.selected.ActDate + ' ' + this.selected.ActTime); // beware: month 0 = january, 11 = december
      var endDate = new Date(this.selected.ActDate + ' ' + this.selected.ActTime);
      var title = this.selected.Name + this.selected.AreaName;
      var eventLocation = this.selected.ActPosition;
      var notes = '';
      var success = function(message) { console.log("Success: " + JSON.stringify(message)); };
      var error = function(message) { console.log("Error: " + message); };
      if(window['plugins'].calendar){
        if(this.platform == 'android'){
          window['plugins'].calendar.createEventInteractively(title,eventLocation,notes,startDate,endDate,success,error);
        }else{
          //ios 加一天
          var _addStart = this.AddDays(startDate,1);
          var _addEnd = this.AddDays(endDate,2);
          window['plugins'].calendar.createEventInteractively(title,eventLocation,notes,_addStart,_addEnd,success,error);
        }
      }
    }
  }
  AddDays(date, days) {
    var result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
  }
}
