import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';

@Component({
  selector: 'find-component',
  templateUrl: './find.component.html'
})
export class FindComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    this.topBar.set({
      bg:'',
      header:'mainbg',
      txt:'D11.001',
      backfun:()=>{
        this.backpage();
      }
    });
  }
  ngOnInit() {

  }
  
}
