import { Component, OnInit } from '@angular/core';
import { BaseComponent } from '../../share/component/base.component';
declare var TweenMax;
declare var device;
declare var jslib;
@Component({
  selector: 'init-component',
  templateUrl: './init.component.html'
})
export class InitComponent extends BaseComponent implements OnInit  {

  constructor(  
  ) {
    super();
    //'http://titanstone.webmaster.net.tw/Home/Share?type=seminar&id=3';
    var redirect = jslib.GetStorage('redirect');
    if(redirect && redirect != ''){
      console.log('redirect',redirect);
      var _url = redirect.split('?');
      if(_url.length == 2){
        var param = _url[1];
        var _p = param.split('&');
        if(_p.length == 2){
          var _type = _p[0].replace('type=','');
          var _id = _p[1].replace('id=','');
          //case 地產
          if(_type == 'estateitem' && Number(_id) > 0){
            this.router.navigate(['/layout/estate/item/' + _id]);
          }
          //case 趨勢新聞
          if(_type == 'trend' && Number(_id) > 0){
            this.router.navigate(['/layout/trend/detail/' + _id]);
          }
          //case 說明會
          if(_type == 'seminar' && Number(_id) > 0){
            this.router.navigate(['/layout/seminar/detail/' + _id]);
          }
          //case 官方新聞
          if(_type == 'news' && Number(_id) > 0){
            this.router.navigate(['/layout/news/detail/' + _id]);
          }
        }
      }
      jslib.SetStorage('redirect','');
    }
    
  }
bgviewclass = '';  
ngOnInit() {
  setTimeout(()=>{
    TweenMax.to("#toHome",1, {x:"+=10", yoyo:true, repeat:-1});
  },250);
  setTimeout(()=>{
    this.bgviewclass = 'animated fadeOut';
    setTimeout(()=>{
      this.bgviewclass = 'none';
    },1000);
  },1500);
}
init(){
  var user = this.jslib.GetStorage('user');
  if(user){
    this.router.navigate(['/layout/home']);
  }else{
    this.router.navigate(['/intro']);
  }
}
  
}
