var cordovaEx = {};

cordovaEx.init = function(){
	
  if(cordova.platformId == 'android') {
		//Full Screen
		StatusBar.overlaysWebView(true);
		//android屏蔽返回功能
	  document.addEventListener("backbutton", function(){console.log('backEvent');}, false);
	}
	 //推播事件test
	//  window['JPush'].isPushStopped(function(r){
	//    console.log('isPushStopped',r);
	//  })
	//  console.log('jpush exist?',window['jpush']);
	//  document.addEventListener("jpush.receiveNotification",  (event) => {
	//    console.log('[cordovaEx] GET PUSH receiveNotification ',event);
	//  }, false);
	//  document.addEventListener("jpush.openNotification",  (event) => {
	//    console.log('[cordovaEx] GET PUSH openNotification ',event);
	//  }, false);
	//  document.addEventListener("jpush.backgroundNotification",  (event) => {
	//    console.log('[cordovaEx] GET PUSH backgroundNotification ',event);
	//  }, false);
	//  document.addEventListener("jpush.receiveMessage",  (event) => {
	//    console.log('[cordovaEx] GET PUSH receiveMessage ',event);
	//  }, false);
	//  document.addEventListener("jpush.receiveLocalNotification",  (event) => {
	//    console.log('[cordovaEx] GET PUSH receiveLocalNotification ',event);
	//  }, false);

	 //推播事件
	//  document.addEventListener("jpush.receiveNotification", function (event) {
	// 	console.log('GET PUSH',event);
	// 	if(device.platform == "Android") {

	// 		var _extra = {};
	// 		if(event['extras']){
	// 			_extra['dateStr'] = event['extras']['dateStr'];
	// 			_extra['typeid'] = event['extras']['typeid'];
	// 			_extra['name'] = event['extras']['name'];
	// 		}
	// 	} else {
			
	// 		var jmsg = event.aps.alert;
	// 		var _extra = {};
	// 		if(event['extras']){
	// 			_extra['dateStr'] = event['extras']['dateStr'];
	// 			_extra['typeid'] = event['extras']['typeid'];
	// 			_extra['name'] = event['extras']['name'];
	// 		}
	// 	}
	// 	console.log('cordovaEx.PushEvent:',cordovaEx.PushEvent);
	// 	if(typeof cordovaEx.PushEvent == 'function'){
	// 		console.log('Run PushEvent:',cordovaEx.PushEvent);
	// 		cordovaEx.PushEvent();
	// 	}
	// }, false);
	//檢查地圖類別
	cordovaEx.CheckMap();
	cordovaEx.GetLang();
};
cordovaEx.CheckMap = function (){
   //default
   var map = '';
   if(device.platform == "Android"){

	  //預設 google 
		map = 'https://maps.google.com/maps?q=';
		console.log('set map',map);
		jslib.SetStorage('map',map);
		//var baiduUrl = 'baidumap://map/navi?query=';
		//'package':'com.google.android.apps.maps'
		var isBaidu = startApp.set({
			'package':'com.baidu.BaiduMap'
		});
		isBaidu.check((res)=>{
			console.log('set isBaidu','baidumap://map/geocoder?src=andr.baidu.openAPIdemo&address=');
			jslib.SetStorage('map','baidumap://map/geocoder?src=andr.baidu.openAPIdemo&address=');
		},(err)=>{
			console.log('map check',err)
		});

   }else{
		jslib.SetStorage('map','maps:');
		//test baidu
		var baiduUrl = 'baidumap://map/geocoder?src=andr.baidu.openAPIdemo&address=';
		var buidu = startApp.set(baiduUrl);
		buidu.check((res)=>{
			jslib.SetStorage('map',baiduUrl);
		},(err)=>{
			console.log('map check',err)
		});
   }
   

};
cordovaEx.PushEvent = function(){
	console.log('PushEvent');
}
cordovaEx.GetLang = function(){
	console.log('GetLang');
};
//lang zh_CN/en_US
cordovaEx.GetLang = function(){
		var defaultLang = jslib.GetStorage('lang');
		if(defaultLang){

		}else{
			jslib.SetStorage('lang','zh_CN');
			if(navigator){
					navigator.globalization.getPreferredLanguage((r)=>{
						console.log('getPreferredLanguage',r);
						if(r){
							var _lang = r['value'];
							if((_lang.toLowerCase()).indexOf('en') > -1){
								jslib.SetStorage('lang','en_US');
							}
						}
					});
			}
		}
}
// //Deep Link
function handleOpenURL(url) {
	setTimeout(function() {
	  console.log("on cordovaEx received url: " + url);
	  jslib.SetStorage('redirect',url);
	}, 3000);
}