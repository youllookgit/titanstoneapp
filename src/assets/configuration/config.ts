// == 基礎設定檔 == //
export const Config = {
    VERSION : '5.18.0',  //版本號
    DefaultLang: 'zh_CN', //預設語系
    DevUrl : 'http://localhost:12005/App/',
    releaseUrl:'http://titanstone.webmaster.net.tw/App/',
    //releaseUrl:'https://ap.titanstonegroup.com/App/',
    // CordovaMode: false,
    // DebugMode:true
    CordovaMode: true,
    DebugMode:false
  }
