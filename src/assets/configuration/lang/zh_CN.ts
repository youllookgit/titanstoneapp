﻿let lang_obj = {};
lang_obj["key"] = {
  "Lang" : "zh_CN",
};
lang_obj["Tab"] = {
  "001" : "首页",
  "002" : "我的投资",
  "003" : "产品",
  "004" : "發現",
  "005" : "更多",
};
lang_obj["A10"] = {
  "001" : "诚信 专业 安心",
  "002" : "提供完整且优质的财务管理服务",
};
lang_obj["A11"] = {
  "001" : "不凡品味",
  "002" : "巨石地产详细介绍",
  "003" : "领先业界",
  "004" : "新知趋势、精选活动、官方消息",
  "005" : "资产掌控",
  "006" : "我的地产、基金、保险",
  "007" : "专业咨询",
  "008" : "投资、申购、出售",
  "009" : "注册",
  "010" : "已有巨石帐号",
  "011" : "立即体验",
};
lang_obj["A12"] = {
  "001" : "诚信 专业 安心",
  "002" : "提供完整且优质的财务管理服务",
  "003" : "关于巨石",
  "004" : "巨石精选",
  "005" : "理财趋势",
  "006" : "更多",
  "007" : "精选活动",
  "008" : "活动",
  "009" : "说明会",
  "010" : "讲座",
  "011" : "考察",
  "012" : "官方消息",
  "013" : "请选择您的分享方式",
};
lang_obj["A13"] = {
  "001" : "通知",
  "002" : "全部已读",
  "003" : "全部清空",
  "004" : "工程繳款",
  "005" : "房租收款",
  "006" : "物业产品",
  "007" : "理财趋势",
  "008" : "精选活动",
  "009" : "官方消息",
  "010" : "系统",
  "011" : "您将删除所有的纪录?",
  "012" : "确认",
  "013" : "取消",
  "014" : "确认",
};
lang_obj["A131"] = {
  "001" : "暂无消息",
};
lang_obj["A14"] = {
  "001" : "请选择您的谘询方式",
  "002" : "线上客服谘询",
  "003" : "联络我们",
};
lang_obj["A15"] = {
  "001" : "联络我们",
  "002" : "请填写以下内容,我们会尽速与您联系！",
  "003" : "联络人姓名",
  "004" : "请输入联络人姓名",
  "005" : "电子邮件",
  "006" : "请输入电子邮件",
  "007" : "国籍/地区",
  "008" : "请输入国籍/地区",
  "009" : "联络电话",
  "010" : "请输入联络电话",
  "011" : "联络主题",
  "012" : "请选择主题",
  "013" : "选择项目",
  "014" : "请选择项目",
  "015" : "联络时段",
  "016" : "请选择联络时段",
  "017" : "问题详述",
  "018" : "请输入文字",
  "019" : "必填",
  "020" : "发送",
  "021" : "联络人姓名为必填",
  "022" : "电子邮件格式错误",
  "023" : "必填,请输入8-12数字",
  "024" : "请选择资产项目",
  "025" : "请输入Line/Wechat ID",
};
lang_obj["A16"] = {
  "001" : "注册",
  "002" : "帐号(电子邮件)",
  "003" : "请输入电子邮件",
  "004" : "设定密码",
  "005" : "请输入6-16码英文或数字",
  "006" : "再次确认密码",
  "007" : "请再次输入密码",
  "008" : "注册即同意使用条款及个人资料保护声明",
  "009" : "注册",
  "010" : "已有巨石帐号",
  "011" : "或",
  "012" : "访客浏览",
  "013" : "格式错误,请重新确认",
  "014" : "请输入6-16码英文或数字",
  "015" : "密码不一致,请重新确认",
  "016" : "注册即同意",
  "017" : "使用条款",
  "018" : "及",
  "019" : "个人资料保护声明",
};
lang_obj["A17"] = {
  "001" : "基本资料",
  "002" : "為确保您的会员权益,请正确填写以下资料",
  "003" : "姓名",
  "004" : "请输入姓名",
  "005" : "身分证号码/ID",
  "006" : "请输入身分证号码/ID",
  "007" : "生日",
  "008" : "请选择日期",
  "009" : "国籍/地区",
  "010" : "请选择国籍/地区",
  "011" : "联络电话",
  "012" : "国码",
  "013" : "电话号码",
  "014" : "必填",
  "015" : "必填,8-12數字",
  "016" : "不能输入大于今日的日期",
};
lang_obj["A18"] = {
  "001" : "投资意向",
  "002" : "略过",
  "003" : "请回答以下问题,让我们更快掌握您的需求",
  "004" : "1. 您曾经投资过哪些产品？",
  "005" : "基金",
  "006" : "房地产",
  "007" : "保险",
  "008" : "其他",
  "009" : "2. 请问您是否有海外投资经验及规划？",
  "010" : "是",
  "011" : "否",
  "012" : "3. 请问您目前看好的投资目标市场？(可多选)",
  "013" : "美洲市场",
  "014" : "欧洲市场",
  "015" : "亚洲市场",
  "016" : "全球新兴市场",
  "017" : "4. 请问您有曾经投资过东南亚哪些国家？(可多选)",
  "018" : "柬埔寨",
  "019" : "越南",
  "020" : "马来西亚",
  "021" : "泰国",
  "022" : "菲律宾",
  "023" : "其它",
  "024" : "5. 请问您购买海外地产时,最重视哪些因素？(可多选)",
  "025" : "能否安全退场",
  "026" : "项目地点",
  "027" : "开发商实力",
  "028" : "首付款项",
  "029" : "6. 请问您作保险规划时,优先考虑的因素？(可多选)",
  "030" : "税务规划",
  "031" : "储蓄报酬",
  "032" : "资产传承",
  "033" : "意外理赔",
  "034" : "7. 请问您是否会主动与理财专员做资产检视及咨询？",
  "035" : "是",
  "036" : "否",
  "037" : "下一步",
};
lang_obj["A19"] = {
  "001" : "邮件验证",
  "002" : "注册成功",
  "003" : "请至 abc12345@gmail.com 收取验证信并开通帐号,即可登入!",
  "004" : "立即登入",
  "005" : "首页",
};
lang_obj["A111"] = {
  "001" : "帐号(电子邮件)",
  "002" : "请输入电子邮件",
  "003" : "密碼",
  "004" : "请输入密码",
  "005" : "登入即同意使用条款及个人资料保护声明",
  "006" : "登入",
  "007" : "建立巨石帐号",
  "008" : "忘记密码",
  "009" : "或",
  "010" : "访客浏览",
  "011" : "必填",
  "012" : "格式错误,请重新确认",
  "013" : "帐号或密码错误,请重新确认",
  "014" : "帐号尚未完成验证",
  "015" : "验证信已重新发送至您的邮箱,请点击信中连结完成验证。",
  "016" : "登入即同意",
  "017" : "使用条款",
  "018" : "及",
  "019" : "登入即同意使用条款及个人资料保护声明",
};
lang_obj["A112"] = {
  "001" : "忘记密码",
  "002" : "帐号(电子邮件)",
  "003" : "请输入电子邮件",
  "004" : "确认",
  "005" : "必填",
  "006" : "格式错误,请重新确认",
};
lang_obj["A113"] = {
  "001" : "重设密码邮件",
  "002" : "请至 abc12345@gmail.com 收取重设密码邮件",
  "003" : "点击信中连结并完成验证后,画面会自动跳转。",
  "004" : "没有收到,再寄一次",
};
lang_obj["A114"] = {
  "001" : "重设密码",
  "002" : "设定新密码",
  "003" : "请输入6-16碼英文或數字",
  "004" : "确认新密码",
  "005" : "请再次输入密码",
  "006" : "確認",
  "007" : "必填",
  "008" : "格式错误,请重新确认",
  "009" : "密码不一致,请重新确认",
  "010" : "密码更新成功,请重新登入！",
};
lang_obj["B11"] = {
  "001" : "我的資产",
  "002" : "地产",
  "003" : "基金",
  "004" : "保险",
  "005" : "提示",
  "006" : "您尚未开通此功能！请先与巨石联络并申请开通。",
  "007" : "先不用,谢谢、前往联络我们",
  "008" : "先不用,谢谢",
  "009" : "前往联络我们",
};
lang_obj["B21"] = {
  "001" : "我的地产",
  "002" : "当年度累积收益",
  "003" : "总收益",
  "004" : "包租中",
  "005" : "出租中",
  "006" : "待租中",
  "007" : "未交屋",
};
lang_obj["B211"] = {
  "001" : "您尚未持有相关地产,找找合适的投资物件吧",
  "002" : "我有兴趣",
};
lang_obj["B221"] = {
  "001" : "包租中",
  "002" : "出租中",
  "003" : "待租中",
  "004" : "未交屋",
  "005" : "工程进度",
  "006" : "地产资讯",
  "007" : "缴款资讯",
  "008" : "购买总价",
  "009" : "已缴交金额",
  "010" : "本期应付金额",
  "011" : "缴款期限",
  "012" : "尚未缴款",
  "013" : "已缴款",
  "014" : "当前进度",
  "015" : "第X期",
  "016" : "完工日",
  "017" : "预估完工日",
  "018" : "施工纪录",
};
lang_obj["B2211"] = {
  "001" : "施工纪录",
  "002" : "取消",
};
lang_obj["B222"] = {
  "001" : "包租中",
  "002" : "出租中",
  "003" : "待租中",
  "004" : "未交屋",
  "005" : "工程进度",
  "006" : "地产资讯",
  "007" : "房产地址",
  "008" : "单元",
  "009" : "单元面积",
  "010" : "套内面积",
  "011" : "公设面积",
  "012" : "购买价格",
  "013" : "车位价格",
  "014" : "我的合约",
};
lang_obj["B2221"] = {
  "001" : "收益计算",
  "002" : "租赁状况",
  "003" : "地产资讯",
  "004" : "房产地址",
  "005" : "单元",
  "006" : "单元面积",
  "007" : "套内面积",
  "008" : "公设面积",
  "009" : "购买价格",
  "010" : "车位价格",
  "011" : "我的合约",
  "012" : "我要出售",
};
lang_obj["B2231"] = {
  "001" : "巨石集团提供完善的代租服务,请洽业务人员或点击下方按钮联络我们。",
};
lang_obj["B2232"] = {
  "001" : "目前正积极招租中,如有疑问请洽客服人员。",
  "002" : "委讬期间",
  "003" : "委託出租合约",
};
lang_obj["B2233"] = {
  "001" : "租客基本资料",
  "002" : "租金",
  "003" : "本期未繳",
  "004" : "本期已繳",
  "005" : "承租人",
  "006" : "性別",
  "007" : "ID/护照",
  "008" : "出生日期",
  "009" : "承租時間",
  "010" : "租赁合约",
  "011" : "委讬期间",
  "012" : "委託出租合约",
};
lang_obj["B2234"] = {
  "001" : "您的物件正由巨石集团承租中,请至收益计算查看您的每月收益。",
  "002" : "委讬期间",
  "003" : "包租合约",
};
lang_obj["B224"] = {
  "001" : "2019 年收益表",
  "002" : "预估年报酬率",
  "003" : "(年收益)結算金額",
  "004" : "月",
  "005" : "收入",
  "006" : "支出",
  "007" : "明細",
  "008" : "我要出租",
};
lang_obj["B2241"] = {
  "001" : "尚無收益紀錄",
};
lang_obj["B31"] = {
  "001" : "我的基金",
  "002" : "总投资成本",
  "003" : "避险基金",
  "004" : "私募基金",
  "005" : "共同基金",
  "006" : "单笔",
  "007" : "定投",
  "008" : "基金单号",
  "009" : "基金名称",
  "010" : "涨跌",
  "011" : "涨跌幅",
  "012" : "投资成本",
  "013" : "基金份额",
  "014" : "当月缴款金额",
  "015" : "尚末缴款",
  "016" : "本期已缴",
  "017" : "本期未缴",
  "018" : "对帐日份额净值",
  "019" : "对帐日市值",
  "020" : "涨跌",
  "021" : "涨跌幅",
  "022" : "累积分紅",
  "023" : "对帐日期",
  "024" : "对账单/年报",
  "025" : "业绩走势",
  "026" : "更新日期",
  "027" : "事务历史记录",
  "028" : "全部",
  "029" : "申购",
  "030" : "赎回",
  "031" : "分红",
  "032" : "订单单号",
  "033" : "订单日期",
  "034" : "赎回份额",
  "035" : "赎回净值",
  "036" : "绩效费",
  "037" : "赎回总金额",
  "038" : "手续费",
  "039" : "申购份额",
  "040" : "申购净值",
  "041" : "申购总金额",
  "042" : "分红金额",
  "043" : "我要申购",
  "044" : "我要赎回",
  "045" : "不含手续费",
  "046" : "不含分红",
  "047" : "含分红",
  "048" : "每月更新",
  "049" : "基金",
  "050" : "您尚未持有相关基金,找找合适的投资物件吧。",
  "051" : "选择对帐单年份",
  "052" : "检视对帐单",
  "053" : "交易明細",
  "054" : "共",
  "055" : "笔",
  "056" : "日期",
  "057" : "检视季报",
};
lang_obj["B41"] = {
  "001" : "我的保单",
  "002" : "总保险金额",
  "003" : "被保险人",
  "004" : "保单状态",
  "005" : "保险金额",
};
lang_obj["B42"] = {
  "001" : "我的保单",
  "002" : "保单状态",
  "003" : "保险金额",
  "004" : "保险资料",
  "005" : "人身保险",
  "006" : "保单生效日",
  "007" : "保障期间",
  "008" : "缴费年期",
  "009" : "缴别",
  "011" : "保费金额",
  "012" : "下期缴费日",
  "013" : "要保人",
  "014" : "被保险人",
  "015" : "负责业务姓名",
  "016" : "负责业务联络电话",
  "017" : "保障",
  "018" : "备注事项",
  "019" : "检视商品建议书",
  "020" : "检视要保书",
  "021" : "保险资料",
  "022" : "保单币别",
};
lang_obj["C11"] = {
  "001" : "巨石产品",
  "002" : "预售",
  "003" : "成屋",
};
lang_obj["C121"] = {
  "001" : "国家",
  "002" : "建案类别",
  "003" : "详细地址",
  "004" : "楼层规划",
  "005" : "竣工日期",
  "006" : "项目资讯",
  "007" : "设施服务",
  "008" : "楼层规划",
  "009" : "户型平面图",
  "010" : "建案型录",
  "011" : "周边设施",
  "012" : "建案活动",
  "013" : "活动",
  "014" : "说明会",
  "015" : "讲座",
  "016" : "考察",
  "017" : "活动资讯",
  "018" : "分享",
  "019" : "我有兴趣",
  "020" : "项目型录",
  "021" : "2020年,第四季",
  "022" : "项目活动",
};
lang_obj["C122"] = {
  "001" : "设施",
  "002" : "服务",
  "003" : "建案活动",
  "004" : "活动",
  "005" : "说明会",
  "006" : "讲座",
  "007" : "考察",
  "008" : "活动资讯",
  "009" : "分享",
  "010" : "我有兴趣",
};
lang_obj["D11"] = {
  "001" : "发现",
  "002" : "精选活动",
  "003" : "理财趋势",
  "004" : "官方消息",
};
lang_obj["D21"] = {
  "001" : "理财趋势",
  "002" : "全部文章",
  "003" : "财经",
  "004" : "地产",
  "005" : "保险",
  "006" : "尚未登入,登入后即可观看所有巨石理财趋势",
  "007" : "我要登入",
  "008" : "尚無資料可顯示",
};
lang_obj["D22"] = {
  "001" : "相关推荐",
  "002" : "上一则",
  "003" : "下一则",
  "004" : "分享",
};
lang_obj["D31"] = {
  "001" : "精选活动",
  "002" : "活动",
  "003" : "说明会",
  "004" : "讲座",
  "005" : "考察",
  "006" : "全部类型",
  "007" : "全部场次",
};
lang_obj["D32"] = {
  "001" : "建案",
  "002" : "建案资讯",
  "003" : "活动介绍",
  "004" : "活动流程",
  "005" : "如何前往",
  "006" : "其他场次",
  "007" : "分享",
  "008" : "立即报名",
};
lang_obj["D331"] = {
  "001" : "我要报名",
  "002" : "联络人姓名",
  "003" : "请输入联络人姓名",
  "004" : "国籍",
  "005" : "请选择国籍",
  "006" : "电话",
  "007" : "国码",
  "008" : "请输入电话号码",
  "009" : "电子邮件",
  "010" : "请输入电子邮件",
  "011" : "参加人数",
  "012" : "备注内容",
  "013" : "发送",
  "014" : "必填",
  "015" : "我知道了",
};
lang_obj["D332"] = {
  "001" : "发送成功！",
  "002" : "报名表已提交,我们会尽快和您联系！並请留意您的邮箱！",
  "003" : "加入行事曆",
  "004" : "知道了",
  "005" : "您的需求已提交,我们会尽快和您联系！並请留意您的邮箱！"
};
lang_obj["D41"] = {
  "001" : "官方消息",
};
lang_obj["D42"] = {
  "001" : "上一則",
  "002" : "下一則",
  "003" : "分享",
};
lang_obj["E11"] = {
  "001" : "登出",
  "002" : "立即登入",
  "003" : "关于巨石",
  "004" : "基本资料",
  "005" : "修改密码",
  "006" : "投资意向",
  "007" : "通知设定",
  "008" : "常见问题",
  "009" : "语言设定",
  "010" : "版本",
};
lang_obj["E31"] = {
  "001" : "基本资料内容",
  "002" : "姓名",
  "003" : "帐号(电子邮件)",
  "004" : "身份证号码/ID",
  "005" : "生日",
  "006" : "请选择生日",
  "007" : "国籍/地区",
  "008" : "请选择国籍",
  "009" : "联络电话",
  "010" : "国码",
  "011" : "请输入电话号码",
  "012" : "储存变更",
  "013" : "必填",
};
lang_obj["E41"] = {
  "001" : "修改密码",
  "002" : "原密码",
  "003" : "请输入原秘碼",
  "004" : "设定新密码",
  "005" : "请输入6-16碼英數字",
  "006" : "确认新密码",
  "007" : "请再次输入密码",
  "008" : "确认变更",
  "009" : "格式错误,请重新确认",
  "010" : "请密码不一致,请重新确认",
  "011" : "密码更新成功,请重新登入！",
  "012" : "重设密码邮件已发送",
};
lang_obj["E51"] = {
  "001" : "投资意向",
  "002" : "请回答以下问题,让我们更快掌握您的需求",
  "003" : "1. 您曾经投资过哪些产品？",
  "004" : "基金",
  "005" : "房地产",
  "006" : "保险",
  "007" : "其他",
  "008" : "2. 请问您是否有海外投资经验及规划？",
  "009" : "是",
  "010" : "否",
  "011" : "3. 请问您目前看好的投资目标市场？(可多选)",
  "012" : "美洲市场",
  "013" : "欧洲市场",
  "014" : "亚洲市场",
  "015" : "全球新兴市场",
  "016" : "4. 请问您有曾经投资过东南亚哪些国家？(可多选)",
  "017" : "柬埔寨",
  "018" : "越南",
  "019" : "马来西亚",
  "020" : "泰国",
  "021" : "菲律宾",
  "022" : "其它",
  "023" : "5. 请问您购买海外地产时,最重视哪些因素？(可多选)",
  "024" : "能否安全退场",
  "025" : "项目地点",
  "026" : "开发商实力",
  "027" : "首付款项",
  "028" : "6. 请问您作保险规划时,优先考虑的因素？(可多选)",
  "029" : "税务规划",
  "030" : "储蓄报酬",
  "031" : "资产传承",
  "032" : "意外理赔",
  "033" : "7. 请问您是否会主动与理财专员做资产检视及咨询？",
  "034" : "是",
  "035" : "否",
  "036" : "储存变更",
  "037" : "送出成功！谢谢您的配合！",
};
lang_obj["E61"] = {
  "001" : "通知设定",
  "002" : "允许通知",
  "003" : "通知项目",
  "004" : "工程缴款通知",
  "005" : "房租收款通知",
  "006" : "物业产品通知",
  "007" : "精选活动",
  "008" : "理财趋势通知",
  "009" : "官方消息通知",
  "010" : "系统",
};
lang_obj["E71"] = {
  "001" : "常见问题",
  "002" : "遇到哪方面的问题？",
  "003" : "巨石提供相关常见问题,让您可以尽快解决遇到的问题",
};
lang_obj["E72"] = {
  "001" : "常见问题",
};
lang_obj["E81"] = {
  "001" : "版本",
  "002" : "Titan Stone 版本",
  "003" : "作業系統",
};
lang_obj["E91"] = {
  "001" : "语言设定",
  "002" : "简体中文",
};
export const ZH_CN_TRANS = lang_obj;
