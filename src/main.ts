import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { Config } from './assets/configuration/config';
declare var cordova;
declare var cordovaEx;
declare var device;
declare var StatusBar;
declare var window;
declare var jslib;

if (environment.production) {
  enableProdMode();
}
const bootstrap = () => {
  platformBrowserDynamic().bootstrapModule(AppModule);
};
console.log('CordovaMode',Config.CordovaMode);
if(Config.CordovaMode){
    document.addEventListener('deviceready', () => {
      console.log('cordova deviceready');
      cordovaEx.init();
      //解決cordova share 套件延遲三秒才收的到的問題
      setTimeout(()=>{
        bootstrap();
      },3100);
      // platformBrowserDynamic().bootstrapModule(AppModule)
      // .catch(err => console.log(err));
  }, false);

}else{
  bootstrap();
  // platformBrowserDynamic().bootstrapModule(AppModule)
  // .catch(err => console.log(err));
}




